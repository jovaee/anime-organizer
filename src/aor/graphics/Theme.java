/**
 * 
 */
package aor.graphics;

import java.awt.Color;

/**
 * @author Asmicor
 *
 */
public class Theme {

	public static Color TEXT_COLOR;
	public static Color BACKGROUND_COLOR;
	
	public static Color WAITING_BASE_COLOR;
	public static Color WAITING_ACCENT_COLOR;
	
	public static Color FINISHED_BASE_COLOR;
	public static Color FINISHED_ACCENT_COLOR;
	
	public static Color DROPPED_BASE_COLOR;
	public static Color DROPPED_ACCENT_COLOR;
	
	public static Color STALLED_BASE_COLOR;
	public static Color STALLED_ACCENT_COLOR;
	
	public static Color UPCOMMING_BASE_COLOR;
	public static Color UPCOMMING_ACCENT_COLOR;
	
	public static Color PLANNING_BASE_COLOR;
	public static Color PLANNING_ACCENT_COLOR;
	
	public static final void loadDefaultTheme() {
		TEXT_COLOR = Color.BLACK;
		BACKGROUND_COLOR = Color.WHITE;
		
		WAITING_BASE_COLOR = new Color(0, 102, 153);
		WAITING_ACCENT_COLOR = new Color(100, 149, 237);
		
		FINISHED_BASE_COLOR = new Color(0, 153, 0);
		FINISHED_ACCENT_COLOR = new Color(51, 204, 0);
		
		DROPPED_BASE_COLOR = new Color(204, 0, 0);
		DROPPED_ACCENT_COLOR = new Color(255, 0, 0);
		
		STALLED_BASE_COLOR = new Color(255, 51, 0);
		STALLED_ACCENT_COLOR = new Color(255, 102, 0);
		
		PLANNING_BASE_COLOR = Color.GRAY;
		PLANNING_ACCENT_COLOR = Color.LIGHT_GRAY;
		
		UPCOMMING_BASE_COLOR = new Color(153, 0, 153);
		UPCOMMING_ACCENT_COLOR = new Color(153, 0, 255);
	}
	
	/**
	 * @deprecated
	 * 
	 */
	public static final void loadDarkTheme() {
		
	}
}
