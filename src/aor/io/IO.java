package aor.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import aor.main.Main;
import aor.objects.Anime;
import aor.objects.enums.State;
import aor.objects.enums.Types;

/**
 * @author Asmicor
 *
 */
public class IO {

    /**
     * Update data of stored files to fit the new format. Reads old format and
     * writes the the same data in the new format Note: Not all data will be
     * correctly transfered but most important parts will
     */
    public static void updateData() {

	try {
	    File animeFile;
	    BufferedReader mainScan = new BufferedReader(
		    new FileReader(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\data.txt"));
	    BufferedReader animeScan;

	    int totalEntries = Short.parseShort(mainScan.readLine());
	    int count = 1;

	    StringBuilder builder;
	    String name, altName, link, prequel, sequel, tmp, desc;
	    State state;
	    Types type;
	    File pic;
	    double rating;
	    int episode, totEpisodes, episodeTime, day, month, year, airDay;
	    boolean ongoing;

	    String nameStuff;
	    while ((nameStuff = mainScan.readLine()) != null) {
		/************************************
		 * READ OLD DATA
		 ****************************************************/
		System.out.printf("Updating: %s\n", nameStuff);
		nameStuff = stripChars(nameStuff);

		animeScan = new BufferedReader(new FileReader(
			System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + nameStuff + ".txt"));

		// Read info for specific anime
		name = animeScan.readLine();
		altName = animeScan.readLine();

		prequel = animeScan.readLine();
		sequel = animeScan.readLine();

		link = animeScan.readLine();

		tmp = animeScan.readLine();
		if (tmp.equals("...")) {
		    pic = null;
		} else {
		    pic = new File(tmp);
		}

		ongoing = Boolean.parseBoolean(animeScan.readLine());
		try {
		    tmp = animeScan.readLine();

		    switch (tmp) {
		    case "Monday":
			airDay = 1;
			break;
		    case "Thuesday":
			airDay = 2;
			break;
		    case "Wednesday":
			airDay = 3;
			break;
		    case "Thursday":
			airDay = 4;
			break;
		    case "Friday":
			airDay = 5;
			break;
		    case "Saturday":
			airDay = 6;
			break;
		    case "Sunday":
			airDay = 7;
			break;

		    default:
			airDay = 0;
		    }
		} catch (Exception e) {
		    airDay = 0;
		}

		tmp = animeScan.readLine();
		switch (tmp) {
		case "TV":
		    type = Types.TV;
		    break;
		case "OVA":
		    type = Types.OVA;
		    break;
		case "ONA":
		    type = Types.ONA;
		    break;
		case "Special":
		    type = Types.SPECIAL;
		    break;
		case "Movie":
		    type = Types.MOVIE;
		    break;

		default:
		    type = Types.OVA;
		    break;
		}

		tmp = animeScan.readLine();
		switch (tmp) {
		case "Watching":
		    state = State.WATCHING;
		    break;
		case "Finished":
		    state = State.FINISHED;
		    break;
		case "Dropped":
		    state = State.DROPPED;
		    break;
		case "Stalled":
		    state = State.STALLED;
		    break;
		case "Planning to watch":
		    state = State.PLANNING_TO_WATCH;
		    break;
		case "Upcomming":
		    state = State.UPCOMMING;
		    break;

		default:
		    state = State.WATCHING;
		    break;
		}

		try {
		    day = Integer.parseInt(animeScan.readLine());
		} catch (Exception e) {
		    day = 0;
		}

		try {
		    month = Integer.parseInt(animeScan.readLine());
		} catch (Exception e) {
		    month = 0;
		}

		try {
		    year = Integer.parseInt(animeScan.readLine());
		} catch (Exception e) {
		    year = 0;
		}

		episode = Integer.parseInt(animeScan.readLine());
		totEpisodes = Integer.parseInt(animeScan.readLine());

		episodeTime = Integer.parseInt(animeScan.readLine());

		rating = Float.parseFloat(animeScan.readLine());

		// Read description for anime if there is one
		builder = new StringBuilder();
		while ((tmp = animeScan.readLine()) != null) {
		    builder.append(tmp);
		}

		if (builder.toString().equals("No Description Available")) {
		    desc = "...";
		} else {
		    desc = builder.toString();
		}

		animeScan.close();

		/************************************
		 * WRITE UPDATED DATA
		 ****************************************************/
		// .aod ---> ao = Anime Organizer; d = Data
		PrintWriter writer = new PrintWriter(new File(
			System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + nameStuff + ".aod"));

		writer.println("#The primary name of the anime");
		writer.println(name);
		writer.println("#An alternative name for the anime");
		writer.println(altName);
		writer.println("#The prequels of this anime if any");
		writer.println(prequel + ";;");
		writer.println("#The sequels of this anime if any");
		writer.println(sequel + ";;");
		writer.println("#The spin-offs of this anime if any");
		writer.println("...;;");
		writer.println("#The side stories of this anime if any");
		writer.println("...;;");
		writer.println("#The alternative versions of this anime if any");
		writer.println("...;;");
		writer.println("#The MyAnimeList link for this anime");
		writer.println(link);

		writer.println("#The path to the anime-art");
		if (pic == null) {
		    writer.println("...");
		} else {
		    writer.println(pic.getName());
		}

		writer.println("#Is the anime ongoing");
		writer.println(Boolean.toString(ongoing));
		writer.println("#The airday for the anime if is ongoing");
		writer.println(airDay);

		writer.println("#The type of this anime");
		writer.println(type.toString());

		writer.println("#The state of this anime");
		writer.println(state.toString());

		writer.println("#The day the anime will start airing");
		writer.println(day); // 0 means not specified
		writer.println("#The month the anime will start airing");
		writer.println(month); // 0 means not specified
		writer.println("#The year the anime will start airing");
		writer.println(year); // 0 means not specified

		writer.println("#The day the anime will end airing");
		writer.println(0); // 0 means not specified
		writer.println("#The month the anime will end airing");
		writer.println(0); // 0 means not specified
		writer.println("#The year the anime will end airing");
		writer.println(0); // 0 means not specified

		writer.println("#The last episode that was watched");
		writer.println(episode);
		writer.println("#The total number of episodes this anime has");
		writer.println(totEpisodes);
		writer.println("#The length of a episode in minutes");
		writer.println(episodeTime);
		writer.println("#The rating of this anime [5.0 is regarded as default]");
		writer.println(rating);

		writer.println("#The description of this anime");
		writer.print(desc);

		writer.close();
	    }

	    mainScan.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public static void readFile() {

	try {
	    File animeFile;
	    BufferedReader mainScan = new BufferedReader(
		    new FileReader(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\data.txt"));
	    BufferedReader animeScan;

	    int totalEntries = Integer.parseInt(mainScan.readLine());
	    int count = 1;

	    StringBuilder builder;
	    String name, altName, link, tmp;
	    String[] prequel, sequel, spinoff, sidestory, altVersion;
	    State state;
	    Types type;
	    String pic;
	    float rating;
	    int episode, totEpisodes, episodeTime, sday, smonth, syear, eday, emonth, eyear, airDay;
	    boolean ongoing;

	    String nameStuff;

	    while ((nameStuff = mainScan.readLine()) != null) {
		nameStuff = stripChars(nameStuff);

		System.out.printf("Reading File # %d - %s\n", count, nameStuff);

		animeScan = new BufferedReader(new FileReader(
			System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + nameStuff + ".aod"));

		// Read info for specific anime
		animeScan.readLine();
		name = animeScan.readLine();
		animeScan.readLine();
		altName = animeScan.readLine();

		animeScan.readLine();
		prequel = animeScan.readLine().split(";;");

		animeScan.readLine();
		sequel = animeScan.readLine().split(";;");

		animeScan.readLine();
		spinoff = animeScan.readLine().split(";;");

		animeScan.readLine();
		sidestory = animeScan.readLine().split(";;");

		animeScan.readLine();
		altVersion = animeScan.readLine().split(";;");

		animeScan.readLine();
		link = animeScan.readLine();

		animeScan.readLine();
		tmp = animeScan.readLine();
		if (tmp.equals("...")) {
		    pic = "...";
		} else {
		    pic = tmp;
		}

		animeScan.readLine();
		ongoing = Boolean.parseBoolean(animeScan.readLine());
		animeScan.readLine();
		airDay = Integer.parseInt(animeScan.readLine());

		animeScan.readLine();
		type = Types.toType(animeScan.readLine());

		animeScan.readLine();
		state = State.toState(animeScan.readLine());

		animeScan.readLine();
		sday = Integer.parseInt(animeScan.readLine());
		animeScan.readLine();
		smonth = Integer.parseInt(animeScan.readLine());
		animeScan.readLine();
		syear = Integer.parseInt(animeScan.readLine());

		animeScan.readLine();
		eday = Integer.parseInt(animeScan.readLine());
		animeScan.readLine();
		emonth = Integer.parseInt(animeScan.readLine());
		animeScan.readLine();
		eyear = Integer.parseInt(animeScan.readLine());

		animeScan.readLine();
		episode = Integer.parseInt(animeScan.readLine());
		animeScan.readLine();
		totEpisodes = Integer.parseInt(animeScan.readLine());

		animeScan.readLine();
		episodeTime = Integer.parseInt(animeScan.readLine());

		animeScan.readLine();
		rating = Float.parseFloat(animeScan.readLine());

		// Read description for anime if there is one
		animeScan.readLine();
		builder = new StringBuilder();
		while ((tmp = animeScan.readLine()) != null) {
		    builder.append(tmp);
		}

		Anime aa = new Anime(name, altName, builder.toString(), link, prequel, sequel, sidestory, spinoff,
			altVersion, pic, episode, totEpisodes, episodeTime, airDay, sday, smonth, syear, eday, emonth,
			eyear, ongoing, false, rating, state, type);
		aa.xmlAttributeName = aa.getName();

		Main.startupAdd(aa);

		animeScan.close();
		count = count + 1;
	    }

	    mainScan.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Writes a new file with the updated information for <i>updatedEntry</i>
     * 
     * @param oldName
     *            The old name of this anime - can be the same as current name
     * @param updatedEntry
     *            The updated Anime object
     */
    public static void updateEntry(String oldName, Anime updatedEntry) {
	if (!oldName.equals(updatedEntry.getName())) {
	    oldName = stripChars(oldName);

	    File oldFile = new File(
		    System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + oldName + ".txt");
	    oldFile.delete();
	    Main.sort();
	}

	writeFile(updatedEntry);
    }

    /**
     * Writes the data.txt file
     * 
     * @param anime
     * @param numEntries
     */
    public static void writeBaseFile() {
	try {
	    PrintWriter writer = new PrintWriter(
		    new File(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\data.txt"));

	    writer.println(Main.getNumEntries());
	    for (int i = 0; i < Main.getNumEntries(); i = i + 1) {
		writer.println(Main.getAnimeArray()[i].getName());
	    }
	    writer.close();
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Writes the file that corresponds to <i>newAnime</i>
     * 
     * @param newAnime
     */
    public static void writeFile(Anime newAnime) {

	try {
	    String nameStuff = stripChars(newAnime.getName());
	    PrintWriter writer = new PrintWriter(
		    new File(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + nameStuff + ".aod"));

	    writer.println("#The primary name of the anime");
	    writer.println(newAnime.getName());
	    writer.println("#An alternative name for the anime");
	    writer.println(newAnime.getAltName());

	    writer.println("#The prequels of this anime if any");
	    writer.println(newAnime.getPrequelSize());
	    for (int i = 0; i < newAnime.getPrequelSize(); i = i + 1) {
		writer.println(newAnime.getPrequel()[i]);
	    }

	    writer.println("#The sequels of this anime if any");
	    writer.println(newAnime.getSequelSize());
	    for (int i = 0; i < newAnime.getSequelSize(); i = i + 1) {
		writer.println(newAnime.getSequel()[i]);
	    }

	    writer.println("#The MyAnimeList link for this anime");
	    writer.println(newAnime.getLink());

	    writer.println("#The path to the anime-art");
	    if (newAnime.getAnimeArt().equals(Anime.ANIME_ART_EMPTY_VALUE)) {
		writer.println("...");
	    } else {
		writer.println(newAnime.getAnimeArt());
	    }

	    writer.println("#Is the anime ongoing");
	    writer.println(Boolean.toString(newAnime.isOngoing()));
	    writer.println("#The airday for the anime if is ongoing");
	    writer.println(newAnime.getAirDay());

	    writer.println("#The type of this anime");
	    writer.println(newAnime.getType().toString());

	    writer.println("#The state of this anime");
	    writer.println(newAnime.getState().toString());

	    /*
	     * writer.println(
	     * "#The day the anime will be released[Only when upcomming]");
	     * writer.println(newAnime.getDay()); writer.println(
	     * "#The month the anime will be released[Only when upcomming]");
	     * writer.println(newAnime.getMonth()); writer.println(
	     * "#The year the anime will be released[Only when upcomming]");
	     * writer.println(newAnime.getYear());
	     */

	    writer.println("#The last episode that was watched");
	    writer.println(newAnime.getCurrentEpisode());
	    writer.println("#The total number of episodes this anime has");
	    writer.println(newAnime.getTotalEpisodes());
	    writer.println("#The length of a episode in minutes");
	    writer.println(newAnime.getEpisodeRuntime());
	    writer.println("#The rating of this anime [5.0 is regarded as default]");
	    writer.println(newAnime.getRating());

	    writer.println("#The description of this anime");
	    writer.print(newAnime.getDescription());

	    writer.close();

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Removes all invalid characters that Windows does not support in a
     * filename
     * 
     * @param string
     *            The string that has to be stripped
     * @return The stripped string
     */
    public static final String stripChars(String string) {
	string = string.replace('\\', ' ');
	string = string.replace('/', ' ');
	string = string.replace(':', ' ');
	string = string.replace('?', ' ');
	string = string.replace('\"', ' ');
	string = string.replace('*', ' ');
	string = string.replace('<', ' ');
	string = string.replace('>', ' ');
	string = string.replace('|', ' ');

	return string;
    }

    /**
     * Run this to update stored files to new format
     */
    /*
     * public static void main(String args[]) { //updateData(); //writeToXML();
     * //updateXML();
     * 
     * }
     */

    /**********************************************************************************************************************/
    /*
     * XML /
     **********************************************************************************************************************/

    /**
     * Formats a string to fit XML format specifications
     * 
     * @param xml
     *            The XML string to be formated
     * @return The formated XML string
     */
    public static String formatXml(String xml) {
	try {
	    Transformer serializer = SAXTransformerFactory.newInstance().newTransformer();
	    serializer.setOutputProperty(OutputKeys.INDENT, "yes");
	    serializer.setOutputProperty(OutputKeys.METHOD, "xml");
	    serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
	    Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(xml.getBytes())));
	    StreamResult res = new StreamResult(new ByteArrayOutputStream());
	    serializer.transform(xmlSource, res);
	    return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());

	} catch (Exception e) {
	    return xml;
	}
    }

    /**
     * DO NOT USE!!! Was used to figure out XStream
     */
    public static void writeToXML() {
	String[] pre = { "Apple", "Cake" };
	String[] seq = { "Apple", "Cake", "WOHOO" };
	String[] spin = { "Apple", "Cake" };
	String[] alt = { "Apple", "Cake", "DFVSDFDSf" };

	Anime an = new Anime("One Piece", "...", "About a pirate named Luffy", "...", pre, seq, null, spin, alt, "...",
		5, 101, 23, 0, 0, 0, 0, 0, 0, 0, false, false, 10, State.WATCHING, Types.TV);
	Anime an3 = new Anime("One Piece", "...", "About a pirate named Luffy", "...", pre, seq, null, spin, alt, "...",
		5, 101, 23, 0, 0, 0, 0, 0, 0, 0, false, false, 10, State.WATCHING, Types.TV);

	XStream xstream = new XStream(new DomDriver());

	// xstream.alias("Anime", Anime.class);
	xstream.autodetectAnnotations(true);

	String xml = formatXml(xstream.toXML(an));
	String xml3 = formatXml(xstream.toXML(an3));
	// System.out.println(xml);

	Anime an2 = (Anime) xstream.fromXML(xml);
	System.out.println(an2.getName());
	System.out.println(an2.getSideStory());

	try {
	    FileOutputStream fos = null;
	    fos = new FileOutputStream(System.getProperty("user.home") + "\\Desktop\\test.xml");
	    fos.write("<?xml version=\"1.0\"?>\n".getBytes("UTF-8"));
	    fos.close();

	    ObjectOutputStream objectOutputStream = xstream.createObjectOutputStream(
		    new FileOutputStream(System.getProperty("user.home") + "\\Desktop\\test.xml", true));
	    objectOutputStream.writeObject(an);
	    objectOutputStream.writeObject(an3);

	    objectOutputStream.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    /**
     * Write all anime to the XML file
     * 
     * @param anime
     *            The anime to be written
     */
    public static void writeToXML(Anime[] anime) {
	XStream xstream = new XStream(new DomDriver("UTF-8"));
	xstream.autodetectAnnotations(true);

	try {
	    ObjectOutputStream objectOutputStream = xstream.createObjectOutputStream(
		    new FileOutputStream(System.getProperty("user.home") + "\\Desktop\\test.xml"), "root");
	    objectOutputStream.writeChars("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");

	    for (int i = 0; i < Main.getNumEntries(); i = i + 1) {
		System.out.printf("Converting File To XML # %d - %s\n", i + 1, anime[i].getName());
		objectOutputStream.writeObject(anime[i]);
	    }

	    objectOutputStream.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}

    }

    /**
     * Update a single anime entry in the XML file
     * 
     * @param oldXmlAttribute
     *            The previous XML attribute, before it was changed during
     *            update, used to identify <i>newVersion</i>
     * @param newVersion
     *            The updated version of the anime
     */
    public static void updateXML(String oldXmlAttribute, Anime newVersion) {
	try {
	    XStream xstream = new XStream(new DomDriver("UTF-8"));
	    xstream.alias("Anime", Anime.class);
	    xstream.useAttributeFor(Anime.class, "xmlAttributeName");

	    newVersion.xmlAttributeName = newVersion.getName();
	    // String xml = formatXml(xstream.toXML(newVersion));
	    String xml = xstream.toXML(newVersion);
	    System.out.println(xml);

	    // TODO: Change the directory to the correct to place
	    String filepath = System.getProperty("user.home") + "\\Desktop\\test.xml";

	    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	    docFactory.setNamespaceAware(true);
	    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

	    Document doc = docBuilder.parse(filepath);

	    // Get the root element
	    Node root = doc.getFirstChild();

	    // Get the staff element , it may not working if tag has spaces, or
	    // whatever weird characters in front...it's better to use
	    // getElementsByTagName() to get it directly.
	    // Node staff = company.getFirstChild();

	    // Get the staff element by tag name directly
	    // Node anime = doc.getElementsByTagName("Anime").item(0);

	    // Node nn = doc.getElementsByTagName("Anime").item(0);
	    // Element nodenn =
	    // DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new
	    // ByteArrayInputStream(xml.getBytes())).getDocumentElement();

	    Node fragmentNode = docBuilder.parse(new InputSource(new StringReader(xml))).getDocumentElement();
	    fragmentNode = doc.importNode(fragmentNode, true);

	    // Node newNode = doc.createTextNode(xml);
	    // System.out.println(nodenn.toString());
	    // root.replaceChild(fragmentNode, nn);

	    // loop the staff child node
	    NodeList list = doc.getElementsByTagName("Anime");// root.getChildNodes();

	    System.out.println(list.getLength());

	    System.out.printf("Old XmlAttribute - %s\n", oldXmlAttribute);

	    for (int i = 0; i < list.getLength(); i = i + 1) {
		Node node = list.item(i);
		System.out.println(node.getAttributes().item(0).toString());
		if (node.getAttributes().item(0).toString().equals("xmlAttributeName=\"" + oldXmlAttribute + "\"")) {
		    root.replaceChild(fragmentNode, node);
		    System.out.println("AAAAAAAAA I just updated a node");
		    break;
		}

		// get the salary element, and update the value
		/*
		 * if ("name".equals(node.getNodeName())) { node.setTextContent(
		 * "Naruto Shippuden"); }
		 */
	    }

	    Transformer transformer = SAXTransformerFactory.newInstance().newTransformer();
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	    DOMSource source = new DOMSource(doc);
	    StreamResult result = new StreamResult(new File(filepath));

	    transformer.transform(source, result);

	    System.out.println("Done Updating");

	} catch (ParserConfigurationException pce) {
	    pce.printStackTrace();
	} catch (TransformerException tfe) {
	    tfe.printStackTrace();
	} catch (IOException ioe) {
	    ioe.printStackTrace();
	} catch (SAXException sae) {
	    sae.printStackTrace();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * Read the anime from the XML file
     * 
     * @return A list containing all the anime
     */
    @SuppressWarnings("finally")
    public static List<Anime> readFromXML() {
	Anime anime;

	ObjectInputStream objectInputStream;
	XStream xstream;

	List<Anime> list = new ArrayList<Anime>();
	
	try {
	    xstream = new XStream(new DomDriver("UTF-8"));
	    xstream.alias("Anime", Anime.class);
	    // xstream.autodetectAnnotations(true); // For some reason this does not work

	    // TODO: Change the directory to the correct to place
	    objectInputStream = xstream.createObjectInputStream(new FileInputStream(System.getProperty("user.home") + "\\Desktop\\test.xml"));

	    while (true) {
		anime = (Anime) objectInputStream.readObject();
		anime.xmlAttributeName = anime.getName();

		System.out.printf("Reading %s\n", anime.getName());
		list.add(anime);
	    }
	} catch (EOFException eof) {
	    // This exception will always be caught unless the file does not exist.
	    // It is used to detect when all Anime have been read from the data file
	} catch (FileNotFoundException fnfe) {
	    System.err.println("The data.xml file could not be found");
	    System.err.println("Starting Anime Organizer in \"Empty\" mode");
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    return list;
	}
    }

    private static void cleanEmptyTextNodes(Node parentNode) {
	boolean removeEmptyTextNodes = false;
	Node childNode = parentNode.getFirstChild();
	while (childNode != null) {
	    removeEmptyTextNodes |= checkNodeTypes(childNode);
	    childNode = childNode.getNextSibling();
	}

	if (removeEmptyTextNodes) {
	    removeEmptyTextNodes(parentNode);
	}
    }

    private static void removeEmptyTextNodes(Node parentNode) {
	Node childNode = parentNode.getFirstChild();
	while (childNode != null) {
	    // grab the "nextSibling" before the child node is removed
	    Node nextChild = childNode.getNextSibling();

	    short nodeType = childNode.getNodeType();
	    if (nodeType == Node.TEXT_NODE) {
		boolean containsOnlyWhitespace = childNode.getNodeValue().trim().isEmpty();
		if (containsOnlyWhitespace) {
		    parentNode.removeChild(childNode);
		}
	    }
	    childNode = nextChild;
	}
    }

    private static boolean checkNodeTypes(Node childNode) {
	short nodeType = childNode.getNodeType();

	if (nodeType == Node.ELEMENT_NODE) {
	    cleanEmptyTextNodes(childNode); // recurse into subtree
	}

	if (nodeType == Node.ELEMENT_NODE || nodeType == Node.CDATA_SECTION_NODE || nodeType == Node.COMMENT_NODE) {
	    return true;
	} else {
	    return false;
	}
    }

}
