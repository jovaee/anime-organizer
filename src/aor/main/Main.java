package aor.main;

import java.awt.EventQueue;
import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.swing.UIManager;

import aor.graphics.Theme;
import aor.io.IO;
import aor.objects.Anime;
import aor.objects.enums.Mode;
import aor.ui.Creator;
import aor.ui.Error;
import aor.ui.Selector;
import aor.ui.Viewer;

/**
 * @author Asmicor
 *
 */
@SuppressWarnings("unused")
public class Main {

    private static Anime[] anime;

    private static int numEntries = 0;
    /*
     * 0 = nothing 1 = name 2 = state
     */
    private static int searchType = 0;
    /*
     * 0 = Name 1 = AltName
     */
    private static int sortType = 0;

    private static Selector selector;
    private static Viewer viewer;
    private static Creator creator;
    private static Error error;

    public static void addAnime(Anime newAnime) {
	if (anime.length <= numEntries) {
	    resizeArray();
	}

	anime[numEntries] = newAnime;
	numEntries = numEntries + 1;

	sort();
	selector.addAnimeString(anime, numEntries);

	IO.writeFile(newAnime);
    }

    private static int binarySearch(String name) {

	int low = 0;
	int high = numEntries - 1;
	int mid;

	while (low <= high) {
	    mid = low + (high - low) / 2;

	    if (name.toUpperCase().compareTo(anime[mid].getName().toUpperCase()) < 0) {
		high = mid - 1;
	    } else if (name.toUpperCase().compareTo(anime[mid].getName().toUpperCase()) > 0) {
		low = mid + 1;
	    } else {
		return mid;
	    }
	}

	return -1;
    }

    /**
     * 
     * @param name
     *            - The alternative name of the anime that needs to be searched
     *            for
     * @return The index in <i>anime<i> where the anime <i>name<i> is located
     */
    private static int binarySearchAlt(String name) {

	int low = 0;
	int high = numEntries - 1;
	int mid;

	while (low <= high) {
	    mid = low + (high - low) / 2;

	    if (name.toUpperCase().compareTo(anime[mid].getAltName().toUpperCase()) < 0) {
		high = mid - 1;
	    } else if (name.toUpperCase().compareTo(anime[mid].getAltName().toUpperCase()) > 0) {
		low = mid + 1;
	    } else {
		return mid;
	    }
	}

	return -1;
    }

    public static void checkSearchState() {
	if (searchType == 1) {
	    selector.searchNameWise();
	} else if (searchType == 2) {
	    selector.searchStateWise();
	}
    }

    public static void deleteAnime(String name) {
	File file = new File(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\" + name + ".txt");
	file.delete();

	anime[getAnimeIndex(name)] = anime[numEntries - 1];
	numEntries = numEntries - 1;

	sort();
	selector.removeAnimeString(anime, numEntries);

	checkSearchState();

	IO.writeBaseFile();
    }

    public static boolean doesContain(String name) {
	if (binarySearch(name) == -1 && binarySearchAlt(name) == -1) {
	    return false;
	} else {
	    return true;
	}
    }

    /**
     * 
     * @param name
     *            The name of the anime that needs to be searched for
     * @return The Anime object of the anime that was searched for
     */
    public static Anime getAnime(String name) {
	return anime[binarySearch(name)];
    }

    public static Anime[] getAnimeArray() {
	return anime;
    }

    /**
     * 
     * @param name
     *            - The name of the anime that needs to be searched for
     * @return The index in <i>anime<i> where the anime <i>name<i> is located
     */
    public static int getAnimeIndex(String name) {
	return binarySearch(name);
    }

    public static void main(String[] args) {
	// Check if the directory where info is stored exits.
	// If not create it
	if (!(new File(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\Pictures").exists())) {
	    (new File(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\Pictures")).mkdirs();
	}

	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e) {
	}

	Theme.loadDefaultTheme();
	// IO.updateData();
	// IO.readFile();
	// IO.writeToXML(getAnimeArray());	

	// Read the anime from file and store
	List<Anime> tmp = IO.readFromXML();
	
	anime = tmp.toArray(new Anime[tmp.size()]);
	numEntries = anime.length;

	newSelector();
    }

    /**
     * Create a new <i>Creator</i> window
     */
    public static void newCreator() {
	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    creator = new Creator();
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create a new <i>Error</i> window
     * 
     * @param message
     *            The error message that will be displayed
     */
    public static void newError(final String message) {
	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    error = new Error(message);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create a new <i>Selector</i> window
     */
    public static void newSelector() {
	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    selector = new Selector(anime, numEntries);
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    /**
     * Create a new <i>Viewer</i> window
     * 
     * @param name
     *            The title that will be given to the JFrame
     */
    public static void newViewer(final String name, final Mode mode) {
	EventQueue.invokeLater(new Runnable() {
	    @Override
	    public void run() {
		try {
		    if (mode != Mode.CREATE) {
			viewer = new Viewer(getAnime(name), mode);
		    } else {
			viewer = new Viewer(mode);
		    }

		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
    }

    private static void resizeArray() {
	anime = Arrays.copyOf(anime, anime.length * 2);
    }

    public static void setSearchState(int state) {
	searchType = state;
    }

    public static void setSortState(int state) {
	sortType = state;
    }

    public static void sort() {
	if (sortType == 0) {
	    Arrays.sort(anime, Anime.nameComp());
	} else {
	    Arrays.sort(anime, Anime.altNameComp());
	}
    }

    public static int getNumEntries() {
	return numEntries;
    }

    public static void startupAdd(Anime newAnime) {
	if (anime.length <= numEntries) {
	    resizeArray();
	}

	anime[numEntries] = newAnime;
	numEntries = numEntries + 1;
    }

    /**
     * Updates the Anime object in the array and writes it to file
     * 
     * @param oldName
     * @param updatedEntry
     */
    public static void updateEntry(String oldName, Anime updatedEntry) {
	// IO.updateEntry(oldName, updatedEntry);
	IO.updateXML(oldName, updatedEntry);
	updatedEntry.xmlAttributeName = updatedEntry.getName(); // ???

	/*
	 * selector.updateInfoPanel(updatedEntry);
	 * selector.addAnimeString(anime, numEntries); Main.checkSearchState();
	 */
    }

    public static void viewNewAnime(String anime) {
	viewer.viewNemAnime(getAnime(anime));
    }
}