package aor.objects;

import java.util.Arrays;
import java.util.Comparator;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

import aor.objects.enums.State;
import aor.objects.enums.Types;

@XStreamAlias("Anime")
public class Anime {	
	
	public static final String STRING_EMPTY_VALUE = "...";
	public static final String ANIME_ART_EMPTY_VALUE = "/aor/graphics/404.jpg";
	
	public static final String[] DAYS = {"", "Monday", "Thuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
	public static final String[] MONTHS = {"", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	
	public static final int INT_EMPTY_VALUE = 0;
	
	@XStreamAsAttribute
	public String xmlAttributeName;
	
	private String name;	
	private String altName;
	
	private String description;
	private String link;
	private String[] prequel;
	private String[] sequel;
	private String[] sideStory;
	private String[] spinoff;
	private String[] altVersion;
	
	private String animeArt;	
	
	private int currentEpisode;
	private int totalEpisodes;
	private int episodeRuntime;
	
	private int airDay;
	
	private int startAirDay, startAirMonth, startAirYear; //Start date of anime
	private int endAirDay, endAirMonth, endAirYear; //End date of anime
		
	private boolean ongoing;
	private boolean isSideSpin;
	
	private float rating;
	
	private State state;
	private Types type;
	
	public static String parseDay(int day) {
		
		switch (day) {
			case 1: return "Monday";
			case 2: return "Thuesday";
			case 3: return "Wednesday";
			case 4: return "Thursday";
			case 5: return "Friday";
			case 6: return "Saturday";
			case 7: return "Sunday";
	
			default: return "...";
		}		
	}
	
	public static int parseDay(String day) {
		
		switch (day) {
			case "Monday": return 1;
			case "Thuesday": return 2;
			case "Wednesday": return 3;
			case "Thursday": return 4;
			case "Friday": return 5;
			case "Saturday": return 6;
			case "Sunday": return 7;
			
			default: return 0;			
		}
	}
	
	public static String parseMonth(int month) {
		
		switch (month) {
			case 1: return "January";
			case 2: return "Febuary";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";			

			default: return "...";
		}		
	}
	
	/**
	 * Create a new Anime object
	 * 
	 * @param name The name of the anime
	 * @param altName An alternative name for the anime
	 * @param description The description for the anime
	 * @param link
	 * @param prequel The prequel(s) of this anime
	 * @param sequel The sequel(s) of this anime
	 * @param sideStory The side stories of this anime
	 * @param spinoff The spin-offs of this anime
	 * @param altVersion 
	 * @param animeArt
	 * @param currentEpisode The last watched episode
	 * @param totalEpisodes The number of episodes this anime has
	 * @param episodeRuntime The runtime of an episode (in minutes)
	 * @param airDay
	 * @param startAirDay
	 * @param startAirMonth
	 * @param startAirYear
	 * @param endAirDay
	 * @param endAirMonth
	 * @param endAirYear
	 * @param ongoing Is the anime currently airing
	 * @param isSideSpin
	 * @param rating
	 * @param state
	 * @param type
	 */
	public Anime(String name, String altName, String description, String link, String[] prequel, String[] sequel, String[] sideStory, String[] spinoff, String[] altVersion, String animeArt,
			int currentEpisode, int totalEpisodes, int episodeRuntime, int airDay, int startAirDay, int startAirMonth, int startAirYear, int endAirDay, int endAirMonth, int endAirYear,
			boolean ongoing, boolean isSideSpin, float rating, State state, Types type) {
		this.name = name;
		this.altName = altName;
		this.description = description;
		this.link = link;
		this.prequel = prequel;
		this.sequel = sequel;
		this.sideStory = sideStory;
		this.spinoff = spinoff;
		this.altVersion = altVersion;
		this.animeArt = animeArt;
		this.currentEpisode = currentEpisode;
		this.totalEpisodes = totalEpisodes;
		this.episodeRuntime = episodeRuntime;
		this.airDay = airDay;
		this.startAirDay = startAirDay;
		this.startAirMonth = startAirMonth;
		this.startAirYear = startAirYear;
		this.endAirDay = endAirDay;
		this.endAirMonth = endAirMonth;
		this.endAirYear = endAirYear;
		this.ongoing = ongoing;
		this.isSideSpin = isSideSpin;
		this.rating = rating;
		this.state = state;
		this.type = type;
	}
	
	/**
	 * @return the prequel at <i>index<i> else <i>STRING_EMPTY_VALUE<i>
	 */
	public String getPrequelAt(int index) {
		if (getPrequelSize() != 0) {
			return prequel[index];
		} else {
			return STRING_EMPTY_VALUE;
		}		
	}
	
	/**
	 * @return The number of prequels this anime has
	 */
	public int getPrequelSize() {
		if (prequel != null) {
			return prequel.length;
		} else {
			return 0;
		}
	}
	
	/**
	 * @param add another prequel to this anime at the given index
	 */
	public void addPrequelAt(String prequel, int index) {
		if (this.prequel == null) {
			this.prequel = new String[1];
			
			index = 0;
		} else if (this.prequel.length < index) {
			this.prequel = Arrays.copyOf(this.prequel, this.prequel.length + 1);
			
			index = this.prequel.length - 1;
		}
		
		this.prequel[index] = prequel;
	}
	
	/**
	 * @param add another prequel to this anime
	 */
	public void addPrequel(String prequel) {
		if (this.prequel == null) {
			this.prequel = new String[1];	
			
			this.prequel[0] = prequel;
		} else {
			this.prequel = Arrays.copyOf(this.prequel, this.prequel.length + 1);
			
			this.prequel[this.prequel.length - 1] = prequel;
		}		
	}

	/**
	 * @return the sequel
	 */
	public String[] getSequel() {
		return sequel;
	}
	
	/**
	 * @return the sequel at <i>index<i> else <i>STRING_EMPTY_VALUE<i>
	 */
	public String getSequelAt(int index) {
		if (getSequelSize() != 0) {
			return sequel[index];
		} else {
			return STRING_EMPTY_VALUE;
		}		
	}
	
	/**
	 * @return The number of sequels this anime has
	 */
	public int getSequelSize() {
		if (sequel != null) {
			return sequel.length;
		} else {
			return 0;
		}
	}

	/**
	 * @param sequel the sequel to set
	 */
	public void setSequel(String sequel[]) {
		this.sequel = sequel;
	}
	
	/**
	 * @param add another sequel to this anime at the given index
	 */
	public void addSequelAt(String sequel, int index) {
		if (this.sequel == null) {
			this.sequel = new String[1];
			
			index = 0;
		} else if (this.sequel.length < index) {
			this.sequel = Arrays.copyOf(this.sequel, this.sequel.length + 1);
			
			index = this.sequel.length - 1;
		}
		
		this.sequel[index] = sequel;
	}
	
	/**
	 * @param add another sequel to this anime
	 */
	public void addSequel(String sequel) {
		if (this.sequel == null) {
			this.sequel = new String[1];	
			
			this.sequel[0] = sequel;
		} else {
			this.sequel = Arrays.copyOf(this.sequel, this.sequel.length + 1);
			
			this.sequel[this.sequel.length - 1] = sequel;
		}		
	}

	/*
	 * Different comparators
	 */
	public static final Comparator<Anime> nameComp() {
        return new Comparator<Anime>() {

			@Override
			public int compare(Anime o1, Anime o2) {
				if (o1.name.compareTo(o2.name) > 0) {
					return 1;
				} else if (o1.name.compareTo(o2.name) < 0) {
					return -1;
				} else {
					return 0;
				}
			}            
        };
    }

    public static final Comparator<Anime> altNameComp() {
        return new Comparator<Anime>() {
        	
			@Override
			public int compare(Anime o1, Anime o2) {
				if (o1.altName.compareTo(o2.altName) > 0) {
					return 1;
				} else if (o1.altName.compareTo(o2.altName) < 0) {
					return -1;
				} else {
					return 0;
				}
			}           
        };
    }
    
    public static final Comparator<Anime> ratingComp() {
        return new Comparator<Anime>() {
        	
			@Override
			public int compare(Anime o1, Anime o2) {
				if (o1.getRating() > o2.getRating()) {
					return 1;
				} else if (o1.getRating() < o2.getRating()) {
					return -1;
				} else {
					return 0;
				}
			}           
        };
    }
    
    public static final Comparator<Anime> stateComp() {
        return new Comparator<Anime>() {
        	
			@Override
			public int compare(Anime o1, Anime o2) {				
				return 0;
			}           
        };
    }
    
    public static final Comparator<Anime> typeComp() {
        return new Comparator<Anime>() {
        	
			@Override
			public int compare(Anime o1, Anime o2) {				
				return 0;
			}           
        };
    }
    
    public static final Comparator<Anime> runtimeComp() {
        return new Comparator<Anime>() {
        	
			@Override
			public int compare(Anime o1, Anime o2) {
				if (o1.getEpisodeRuntime() > o2.getEpisodeRuntime()) {
					return 1;
				} else if (o1.getEpisodeRuntime() < o2.getEpisodeRuntime()) {
					return -1;
				} else {
					return 0;
				}
			}           
		};
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the altName
	 */
	public String getAltName() {
		return altName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @return the prequel
	 */
	public String[] getPrequel() {
		return prequel;
	}

	/**
	 * @return the sideStory
	 */
	public String[] getSideStory() {
		return sideStory;
	}

	/**
	 * @return the spinoff
	 */
	public String[] getSpinoff() {
		return spinoff;
	}

	/**
	 * @return the altVersion
	 */
	public String[] getAltVersion() {
		return altVersion;
	}

	/**
	 * @return the animeArt
	 */
	public String getAnimeArt() {
		return animeArt;
	}

	/**
	 * @return the currentEpisode
	 */
	public int getCurrentEpisode() {
		return currentEpisode;
	}

	/**
	 * @return the totalEpisodes
	 */
	public int getTotalEpisodes() {
		return totalEpisodes;
	}

	/**
	 * @return the episodeRuntime
	 */
	public int getEpisodeRuntime() {
		return episodeRuntime;
	}

	/**
	 * @return the airDay
	 */
	public int getAirDay() {
		return airDay;
	}

	/**
	 * @return the startAirDay
	 */
	public int getStartAirDay() {
		return startAirDay;
	}

	/**
	 * @return the startAirMonth
	 */
	public int getStartAirMonth() {
		return startAirMonth;
	}

	/**
	 * @return the startAirYear
	 */
	public int getStartAirYear() {
		return startAirYear;
	}

	/**
	 * @return the endAirDay
	 */
	public int getEndAirDay() {
		return endAirDay;
	}

	/**
	 * @return the endAirMonth
	 */
	public int getEndAirMonth() {
		return endAirMonth;
	}

	/**
	 * @return the endAirYear
	 */
	public int getEndAirYear() {
		return endAirYear;
	}

	/**
	 * @return the ongoing
	 */
	public boolean isOngoing() {
		return ongoing;
	}

	/**
	 * @return the isSideSpin
	 */
	public boolean isSideSpin() {
		return isSideSpin;
	}

	/**
	 * @return the rating
	 */
	public float getRating() {
		return rating;
	}

	/**
	 * @return the state
	 */
	public State getState() {
		return state;
	}

	/**
	 * @return the type
	 */
	public Types getType() {
		return type;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param altName the altName to set
	 */
	public void setAltName(String altName) {
		this.altName = altName;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @param prequel the prequel to set
	 */
	public void setPrequel(String[] prequel) {
		this.prequel = prequel;
	}

	/**
	 * @param sideStory the sideStory to set
	 */
	public void setSideStory(String[] sideStory) {
		this.sideStory = sideStory;
	}

	/**
	 * @param spinoff the spinoff to set
	 */
	public void setSpinoff(String[] spinoff) {
		this.spinoff = spinoff;
	}

	/**
	 * @param altVersion the altVersion to set
	 */
	public void setAltVersion(String[] altVersion) {
		this.altVersion = altVersion;
	}

	/**
	 * @param animeArt the animeArt to set
	 */
	public void setAnimeArt(String animeArt) {
		this.animeArt = animeArt;
	}

	/**
	 * @param currentEpisode the currentEpisode to set
	 */
	public void setCurrentEpisode(int currentEpisode) {
		this.currentEpisode = currentEpisode;
	}

	/**
	 * @param totalEpisodes the totalEpisodes to set
	 */
	public void setTotalEpisodes(int totalEpisodes) {
		this.totalEpisodes = totalEpisodes;
	}

	/**
	 * @param episodeRuntime the episodeRuntime to set
	 */
	public void setEpisodeRuntime(int episodeRuntime) {
		this.episodeRuntime = episodeRuntime;
	}

	/**
	 * @param airDay the airDay to set
	 */
	public void setAirDay(int airDay) {
		this.airDay = airDay;
	}

	/**
	 * @param startAirDay the startAirDay to set
	 */
	public void setStartAirDay(int startAirDay) {
		this.startAirDay = startAirDay;
	}

	/**
	 * @param startAirMonth the startAirMonth to set
	 */
	public void setStartAirMonth(int startAirMonth) {
		this.startAirMonth = startAirMonth;
	}

	/**
	 * @param startAirYear the startAirYear to set
	 */
	public void setStartAirYear(int startAirYear) {
		this.startAirYear = startAirYear;
	}

	/**
	 * @param endAirDay the endAirDay to set
	 */
	public void setEndAirDay(int endAirDay) {
		this.endAirDay = endAirDay;
	}

	/**
	 * @param endAirMonth the endAirMonth to set
	 */
	public void setEndAirMonth(int endAirMonth) {
		this.endAirMonth = endAirMonth;
	}

	/**
	 * @param endAirYear the endAirYear to set
	 */
	public void setEndAirYear(int endAirYear) {
		this.endAirYear = endAirYear;
	}

	/**
	 * @param ongoing the ongoing to set
	 */
	public void setOngoing(boolean ongoing) {
		this.ongoing = ongoing;
	}

	/**
	 * @param isSideSpin the isSideSpin to set
	 */
	public void setSideSpin(boolean isSideSpin) {
		this.isSideSpin = isSideSpin;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(float rating) {
		this.rating = rating;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Types type) {
		this.type = type;
	}
}