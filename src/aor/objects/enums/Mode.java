/**
 * The different modes that the Viewer can be in
 */
package aor.objects.enums;

/**
 * @author Asmicor
 *
 */
public enum Mode {
	VIEW, UPDATE, CREATE
}
