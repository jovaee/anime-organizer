package aor.objects.enums;

/**
 * @author Asmicor
 * The different states that an anime can be in
 */

public enum State {
	WATCHING, PLANNING_TO_WATCH, FINISHED, DROPPED, STALLED, UPCOMMING;
	
	public static final String[] STATES = {"Watching", "Planning to Watch", "Finished", "Dropped", "Stalled", "Upcomming"};
	
	public String toString() {
		switch (this) {
			case WATCHING: return "Watching";
			case PLANNING_TO_WATCH: return "Planning to Watch";
			case FINISHED: return "Finished";
			case DROPPED: return "Dropped";
			case STALLED: return "Stalled";
			case UPCOMMING: return "Upcomming";		
			
			default: return "Watching";		
		}
	}
	
	/**
	 * Returns a State version of the string given as parameter
	 * 
	 * @param state The String to be parsed
	 * @return a State with regards too state. If no match WATCHING is returned
	 */
	public static State toState(String state) {
		switch (state) {
			case "Watching": return WATCHING;
			case "Planning to Watch": return PLANNING_TO_WATCH;
			case "Finished": return FINISHED;
			case "Dropped": return DROPPED;
			case "Stalled": return STALLED;
			case "Upcomming": return UPCOMMING;
						
			default: return WATCHING;
		}
	}
}
