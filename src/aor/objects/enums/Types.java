package aor.objects.enums;

/**
 * @author Asmicor
 * The different types of anime that exists
 */
public enum Types {	
	TV, ONA, OVA, SPECIAL, MOVIE;
	
	public static final String[] TYPES = {"TV", "OVA", "ONA", "Special", "Movie"};
	
	public String toString() {
		switch (this) {
			case TV: return "TV";
			case ONA: return "ONA";
			case OVA: return "OVA";
			case SPECIAL: return "Special";
			case MOVIE: return "Movie";
						
			default: return "TV";
		
		}
	}	
	
	/**
	 * Returns a Types version of the string given as parameter
	 * 
	 * @param type The String to be parsed
	 * @return a Types with regards too type. If no match TV is returned
	 */
	public static Types toType(String type) {
		switch (type) {
			case "TV": return TV;
			case "ONA": return ONA;
			case "OVA": return OVA;
			case "Special": return SPECIAL;
			case "Movie": return MOVIE;
						
			default: return TV;
		}
	}
}