/**
 * 
 */
package aor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import aor.main.Main;
import aor.objects.Anime;
import aor.objects.enums.State;
import aor.objects.enums.Types;

/**
 * @author Asmicor
 *
 */
public class Creator extends JFrame {
	
	private static final long serialVersionUID = -5237256098236048721L;
	
	private File pictureFile;
	
	private JButton btnCancel;
	private JButton btnCreate;
	private JButton btnEpisodeMinus;	
	
	private JButton btnEpisodePlus;
	
	private JButton btnEpisodeTimeMinus;
	private JButton btnEpisodeTimePlus;
	private JButton btnRatingMinus;
	private JButton btnRatingPlus;
	private JButton btnTotEpisodesMinus;
	private JButton btnTotEpisodesPlus;
	private JCheckBox chkbOngoing;
	private JComboBox<String> cmbAirDay;
	private JComboBox<Integer> cmbDay;
	private JComboBox<String> cmbMonth;
	private JComboBox<String> cmbState;
	private JComboBox<String> cmbType;
	private JComboBox<Integer> cmbYear;
	private JPanel contentPane;
	private JTextPane fpDesription;
	private JLabel lblAirDay;
	private JLabel lblDescription;
	private JLabel lblEpisode;
	private JLabel lblEpisodeTime;
	private JLabel lblInformation;
	private JLabel lblLink;
	private JLabel lblPrequelData;
	private JLabel lblRating;
	private JLabel lblRatingData;
	private JLabel lblSequelData;
	private JLabel lblState;
	private JLabel lblType;	
	private JPanel pnlButtons;
	private JLabel picture;
	private PSSelector psSelector;
	private boolean startSate = true;
	private JTextField tfAltName;
	private JTextField tfEpisodeData;
	private JTextField tfEpisodeTime;
	private JTextField tfLink;
	private JTextField tfName;
	private JTextField tfTotEpisodesData;
	private JLabel lblPicture;

	
	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked"})
	public Creator() {	
		pictureFile = null;
		
		setTitle("Anime Organizer - Creator");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 943, 845);
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(Color.WHITE);
		scrollPane.setViewportView(panel);
		
		tfName = new JTextField();
		tfName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (startSate == false && tfName.getText().equals("Please type a name...")) {
					tfName.setText("");
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (startSate == false && tfName.getText().equals("")) {
					tfName.setText("Please type a name...");
				}
			}
		});		
		tfName.setBorder(new LineBorder(Color.BLACK, 2, true));
		tfName.setFont(new Font("Tahoma", Font.BOLD, 15));
		tfName.setText("Please type a name...");
		tfName.setForeground(Color.WHITE);
		tfName.setHorizontalAlignment(SwingConstants.CENTER);
		tfName.setColumns(10);
		tfName.setBackground(new Color(0, 102, 153));
		
		tfAltName = new JTextField();
		tfAltName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if (startSate == false && tfAltName.getText().equals("Please type an alternative name...")) {
					tfAltName.setText("");
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if (startSate == false && tfAltName.getText().equals("")) {
					tfAltName.setText("Please type an alternative name...");
				}
			}
		});
		tfAltName.setText("Please type an alternative name...");
		tfAltName.setHorizontalAlignment(SwingConstants.CENTER);
		tfAltName.setForeground(Color.WHITE);
		tfAltName.setFont(new Font("Tahoma", Font.BOLD, 13));
		tfAltName.setColumns(10);
		tfAltName.setBorder(new LineBorder(Color.BLACK, 2, true));
		tfAltName.setBackground(new Color(0, 102, 153));
		
		lblDescription = new JLabel("Description:");
		lblDescription.setOpaque(true);
		lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
		lblDescription.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblDescription.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		lblDescription.setBackground(new Color(100, 149, 237));
		
		picture = new JLabel("Please select a picture...");
		picture.setBorder(null);
		picture.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JFileChooser chooser = new JFileChooser();					
				chooser.showOpenDialog(picture);			
				
				if (chooser.getSelectedFile() != null) {
					pictureFile = chooser.getSelectedFile();				
					picture.setIcon(new ImageIcon(pictureFile.toString()));
					
				}				
			}
		});
		picture.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		picture.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBackground(Color.WHITE);
		
		lblInformation = new JLabel("Information:");
		lblInformation.setOpaque(true);
		lblInformation.setHorizontalAlignment(SwingConstants.CENTER);
		lblInformation.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblInformation.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		lblInformation.setBackground(new Color(100, 149, 237));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(null);
		panel_2.setBackground(Color.WHITE);
		
		JLabel lblNewLabel = new JLabel("Prequel:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JLabel lblSequel = new JLabel("Sequel:");
		lblSequel.setHorizontalAlignment(SwingConstants.LEFT);
		lblSequel.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		cmbDay = new JComboBox();
		cmbDay.setVisible(false);
		cmbDay.setModel(new DefaultComboBoxModel(new String[] {"", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}));
		
		cmbMonth = new JComboBox();
		cmbMonth.setVisible(false);
		cmbMonth.setModel(new DefaultComboBoxModel(new String[] {"", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}));
		
		cmbYear = new JComboBox();
		cmbYear.setVisible(false);
		cmbYear.setModel(new DefaultComboBoxModel(new String[] {"", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025"}));
		
		cmbAirDay = new JComboBox();		
		cmbAirDay.setModel(new DefaultComboBoxModel(new String[] {"", "Monday", "Thuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}));
		cmbAirDay.setEnabled(false);
		
		chkbOngoing = new JCheckBox("");
		chkbOngoing.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				if (!cmbState.getSelectedItem().equals("Upcomming") && !cmbState.getSelectedItem().equals("Finished")) {
					if (chkbOngoing.isSelected()) {						
						cmbAirDay.setEnabled(true);
					} else {						
						cmbAirDay.setEnabled(false);
						cmbAirDay.setSelectedIndex(0);
					}
				} else {
					chkbOngoing.setSelected(false);					
					cmbAirDay.setEnabled(false);
					cmbAirDay.setSelectedIndex(0);
				}				
			}
		});
		
		cmbState = new JComboBox<String>();
		cmbState.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				//Change label color
				if (cmbState.getSelectedItem().equals("Watching")) {
					btnEpisodeMinus.setEnabled(true);
					btnEpisodePlus.setEnabled(true);
					tfEpisodeData.setEnabled(true);
					
					updateColours("Watching");
				} else if (cmbState.getSelectedItem().equals("Finished")) {					
					updateColours("Finished");
					
					//Disable air day capabilities
					chkbOngoing.setSelected(false);					
					
					lblRatingData.setEnabled(true);
					btnRatingMinus.setEnabled(true);
					btnRatingPlus.setEnabled(true);
					
					return;
				} else if (cmbState.getSelectedItem().equals("Dropped")) {
					updateColours("Dropped");
					
					lblRatingData.setEnabled(true);
					btnRatingMinus.setEnabled(true);
					btnRatingPlus.setEnabled(true);
					
					return;
				} else if (cmbState.getSelectedItem().equals("Stalled")) {
					updateColours("Stalled");
				} else if (cmbState.getSelectedItem().equals("Planning to watch")) {
					updateColours("Planning to watch");
					
					btnEpisodeMinus.setEnabled(false);
					btnEpisodePlus.setEnabled(false);
					tfEpisodeData.setEditable(false);
					tfEpisodeData.setText("0");
				} else {
					updateColours("Upcomming");
					
					btnEpisodeMinus.setEnabled(false);
					btnEpisodePlus.setEnabled(false);
					tfEpisodeData.setEditable(false);
					tfEpisodeData.setText("0");
					
					//Set upcomming labels and boxes visible
					cmbDay.setVisible(true);
					cmbMonth.setVisible(true);
					cmbYear.setVisible(true);					
					
					//Disable air day capabilities
					chkbOngoing.setSelected(false);
					cmbAirDay.setEnabled(false);
					cmbAirDay.setSelectedIndex(0);				
					
					return;
				}				
				
				cmbDay.setVisible(false);
				cmbMonth.setVisible(false);
				cmbYear.setVisible(false);				
				
				lblRatingData.setEnabled(false);
				btnRatingMinus.setEnabled(false);
				btnRatingPlus.setEnabled(false);
			}
		});
		
		cmbState.setModel(new DefaultComboBoxModel(new String[] {"Watching", "Planning to watch", "Finished", "Dropped", "Stalled", "Upcomming"}));
		
		lblPrequelData = new JLabel("...");
		lblPrequelData.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrequelData.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblSequelData = new JLabel("...");
		lblSequelData.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSequelData.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JButton btnNewButton = new JButton("Select Prequel");
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				newPS("Prequel", lblPrequelData);				
			}
		});
		
		JButton btnNewButton_1 = new JButton("Select Sequel");
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				newPS("Sequel", lblSequelData);				
			}
		});
		
		lblAirDay = new JLabel("Air Day:");
		lblAirDay.setHorizontalAlignment(SwingConstants.LEFT);
		lblAirDay.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		cmbType = new JComboBox<String>();
		cmbType.setModel(new DefaultComboBoxModel(new String[] {"TV", "OAV", "ONA", "Movie", "Special"}));
		cmbType.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if (cmbType.getSelectedIndex() == 3) {
					if (Integer.parseInt(tfEpisodeData.getText()) > 1) {
						tfEpisodeData.setText("1");
					}					
					tfTotEpisodesData.setText("1");					
					
					btnTotEpisodesMinus.setEnabled(false);
					btnTotEpisodesPlus.setEnabled(false);					
					
					tfTotEpisodesData.setEditable(false);
				} else {					
					btnTotEpisodesMinus.setEnabled(true);
					btnTotEpisodesPlus.setEnabled(true);					
					
					tfTotEpisodesData.setEditable(true);
				}
			}
		});	
		
		lblType = new JLabel("Type:");
		lblType.setHorizontalAlignment(SwingConstants.LEFT);
		lblType.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblState = new JLabel("State:");
		lblState.setHorizontalAlignment(SwingConstants.LEFT);
		lblState.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblEpisode = new JLabel("Episode:");
		lblEpisode.setToolTipText("The last episodes that was watched");
		lblEpisode.setHorizontalAlignment(SwingConstants.LEFT);
		lblEpisode.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		btnEpisodeMinus = new JButton("-");
		btnEpisodeMinus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Integer.parseInt(tfEpisodeData.getText()) > 0) {
					tfEpisodeData.setText(Integer.toString(Integer.parseInt(tfEpisodeData.getText()) - 1));
				}
			}
		});
		
		btnEpisodePlus = new JButton("+");		
		btnEpisodePlus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Integer.parseInt(tfTotEpisodesData.getText()) == 0 || Integer.parseInt(tfEpisodeData.getText()) < Integer.parseInt(tfTotEpisodesData.getText())) {
					tfEpisodeData.setText(Integer.toString(Integer.parseInt(tfEpisodeData.getText()) + 1));
				}	
			}
		});
		
		btnRatingMinus = new JButton("-");
		btnRatingMinus.setEnabled(false);
		btnRatingMinus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Double.parseDouble(lblRatingData.getText()) > 0) {
					lblRatingData.setText(Double.toString(Double.parseDouble(lblRatingData.getText()) - 0.5));
				}
			}
		});
		
		lblRatingData = new JLabel("5.0");
		lblRatingData.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lblRatingData.setEnabled(false);
		lblRatingData.setHorizontalAlignment(SwingConstants.CENTER);
		lblRatingData.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		btnRatingPlus = new JButton("+");
		btnRatingPlus.setEnabled(false);
		btnRatingPlus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Double.parseDouble(lblRatingData.getText()) < 10) {
					lblRatingData.setText(Double.toString(Double.parseDouble(lblRatingData.getText()) + 0.5));
				}
			}
		});
		
		lblRating = new JLabel("Rating:");
		lblRating.setHorizontalAlignment(SwingConstants.LEFT);
		lblRating.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblLink = new JLabel("Link:");
		lblLink.setHorizontalAlignment(SwingConstants.LEFT);
		lblLink.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		tfLink = new JTextField();
		tfLink.setForeground(Color.BLUE);
		tfLink.setColumns(10);
		
		JLabel lblTotEpisodes = new JLabel("Total Episodes:");
		lblTotEpisodes.setToolTipText("The number of episodes that this anime has in total. A zero represents an unknown number");
		lblTotEpisodes.setHorizontalAlignment(SwingConstants.LEFT);
		lblTotEpisodes.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		btnTotEpisodesMinus = new JButton("-");
		btnTotEpisodesMinus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Integer.parseInt(tfTotEpisodesData.getText()) > 0) {
					if (Integer.parseInt(tfTotEpisodesData.getText()) != 0 && Integer.parseInt(tfTotEpisodesData.getText()) > Integer.parseInt(tfEpisodeData.getText())) {
						tfTotEpisodesData.setText(Integer.toString(Integer.parseInt(tfTotEpisodesData.getText()) - 1));			
					} 					
				}
			}
		});
		
		btnTotEpisodesPlus = new JButton("+");
		btnTotEpisodesPlus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				if (Integer.parseInt(tfTotEpisodesData.getText()) < Integer.parseInt(tfEpisodeData.getText())) {
					tfTotEpisodesData.setText(tfEpisodeData.getText());
				} else {
					tfTotEpisodesData.setText(Integer.toString(Integer.parseInt(tfTotEpisodesData.getText()) + 1));				
				}
			}
		});
		
		tfEpisodeData = new JTextField();
		tfEpisodeData.setFont(new Font("Tahoma", Font.BOLD, 12));
		tfEpisodeData.setHorizontalAlignment(SwingConstants.CENTER);
		tfEpisodeData.setText("0");
		tfEpisodeData.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		tfEpisodeData.setColumns(10);
		
		tfTotEpisodesData = new JTextField();
		tfTotEpisodesData.setFont(new Font("Tahoma", Font.BOLD, 12));
		tfTotEpisodesData.setHorizontalAlignment(SwingConstants.CENTER);
		tfTotEpisodesData.setText("0");
		tfTotEpisodesData.setColumns(10);
		tfTotEpisodesData.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		lblEpisodeTime = new JLabel("Episode Time:");
		lblEpisodeTime.setToolTipText("The time of an episode in minutes");
		lblEpisodeTime.setHorizontalAlignment(SwingConstants.LEFT);
		lblEpisodeTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		btnEpisodeTimeMinus = new JButton("-");
		btnEpisodeTimeMinus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Integer.parseInt(tfEpisodeTime.getText()) > 0) {
					tfEpisodeTime.setText(Integer.toString(Integer.parseInt(tfEpisodeTime.getText()) - 1));	
				}
			}
		});
		
		tfEpisodeTime = new JTextField();
		tfEpisodeTime.setText("0");
		tfEpisodeTime.setHorizontalAlignment(SwingConstants.CENTER);
		tfEpisodeTime.setFont(new Font("Tahoma", Font.BOLD, 12));
		tfEpisodeTime.setColumns(10);
		tfEpisodeTime.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		btnEpisodeTimePlus = new JButton("+");
		btnEpisodeTimePlus.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				tfEpisodeTime.setText(Integer.toString(Integer.parseInt(tfEpisodeTime.getText()) + 1));	
			}
		});
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblEpisode, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblNewLabel)
								.addComponent(lblSequel, GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
							.addGap(322)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblSequelData, GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
								.addComponent(lblPrequelData, GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE))
							.addGap(10)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(btnEpisodeMinus)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfEpisodeData, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnEpisodePlus))
								.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(lblLink, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 613, Short.MAX_VALUE)
							.addComponent(tfLink, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(0, 0, Short.MAX_VALUE)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING, false)
											.addComponent(lblType, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblAirDay, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
											.addComponent(lblRating, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 59, Short.MAX_VALUE))
										.addComponent(lblEpisodeTime, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblTotEpisodes, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE))
									.addGap(530))
								.addGroup(Alignment.TRAILING, gl_panel_1.createSequentialGroup()
									.addComponent(lblState, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addGap(318)
									.addComponent(cmbDay, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(cmbMonth, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(cmbYear, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
									.addGap(31)))
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(btnEpisodeTimeMinus, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(tfEpisodeTime, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
									.addGap(6)
									.addComponent(btnEpisodeTimePlus, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
								.addComponent(cmbType, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(btnTotEpisodesMinus, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(tfTotEpisodesData, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnTotEpisodesPlus, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
								.addComponent(cmbState, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(chkbOngoing)
									.addGap(13)
									.addComponent(cmbAirDay, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(btnRatingMinus, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
									.addGap(4)
									.addComponent(lblRatingData, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
									.addGap(4)
									.addComponent(btnRatingPlus, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)))))
					.addGap(15))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(btnNewButton)
						.addComponent(lblPrequelData, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
					.addGap(20)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblSequel, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_1)
								.addComponent(lblSequelData, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnEpisodePlus)
								.addComponent(lblEpisode, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnEpisodeMinus)
							.addComponent(tfEpisodeData, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(15)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnTotEpisodesMinus)
							.addComponent(lblTotEpisodes, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnTotEpisodesPlus)
							.addComponent(tfTotEpisodesData, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
					.addGap(15)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addComponent(lblState, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(lblRating, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(lblAirDay, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(15)
							.addComponent(lblType, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(lblEpisodeTime, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(cmbState, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(cmbYear, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(cmbMonth, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(cmbDay, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(btnRatingMinus)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(3)
									.addComponent(lblRatingData, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
								.addComponent(btnRatingPlus))
							.addGap(10)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(chkbOngoing)
								.addComponent(cmbAirDay, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(cmbType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(btnEpisodeTimeMinus)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(1)
									.addComponent(tfEpisodeTime, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
								.addComponent(btnEpisodeTimePlus))))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLink, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfLink, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(133))
		);
		panel_1.setLayout(gl_panel_1);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(null);
		panel_2.add(scrollPane_1, BorderLayout.CENTER);
		
		fpDesription = new JTextPane();
		fpDesription.setBorder(null);
		fpDesription.setToolTipText("");
		fpDesription.setFont(new Font("Tahoma", Font.PLAIN, 12));
		fpDesription.setBackground(Color.WHITE);
		scrollPane_1.setViewportView(fpDesription);
		
		pnlButtons = new JPanel();
		pnlButtons.setLayout(new GridLayout(0, 2, 0, 0));
		
		btnCreate = new JButton("Create");
		btnCreate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				State state;
				Types type;
				
				//Check TotEpisode vs Episode count
				if (Integer.parseInt(tfTotEpisodesData.getText()) != 0 && (Integer.parseInt(tfEpisodeData.getText()) > Integer.parseInt(tfTotEpisodesData.getText()))) {
					Main.newError("Current episode is larger than max number of episodes");							
					return;
				}
				
				//Check if a name was given to the anime
				if (tfName.getText().equals("Please type a name...")) {
					Main.newError("Please give the anime a name first");					
					return;
				}
				
				//Look if this anime has not already been added
				if (Main.doesContain(tfName.getText()) || Main.doesContain(tfAltName.getText())) {
					Main.newError("This anime if already in the database");					
					return;
				}
				
				//Compares names
				if (tfName.getText().equals(tfAltName.getText())) {
					Main.newError("\"Name\" and \"Alternative\" Name cannot be the same");
					return;
				}		
				
				//Checks episode count vs finished state
				if (Integer.parseInt(tfEpisodeData.getText()) != Integer.parseInt(tfTotEpisodesData.getText()) && cmbState.getSelectedItem().equals("Finished")) {
					Main.newError("State cannot be \"Finished\" when episode count doesn't match");					
					return;
				}
				
				//Checks that state is set to finished when all episodes have been watched
				if (Integer.parseInt(tfTotEpisodesData.getText()) != 0 && Integer.parseInt(tfEpisodeData.getText()) == Integer.parseInt(tfTotEpisodesData.getText()) && !cmbState.getSelectedItem().equals("Finished")) {
					Main.newError("State must be set to \"Finished\" when all episodes have been watched");					
					return;
				}
				
				if (tfLink.getText().equals("")) {
					tfLink.setText("...");
				}
				
				//If there is a picture, move it to the correct directory.
				if (pictureFile != null) {							
					try {
						Path p1 = FileSystems.getDefault().getPath(pictureFile.getPath());
						Path p2 = FileSystems.getDefault().getPath(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\Pictures\\" + pictureFile.getName());
						Files.copy(p1, p2, StandardCopyOption.REPLACE_EXISTING);
						pictureFile = p2.toFile();
					} catch (Exception e) {					
						e.printStackTrace();
					}
				}
				
				switch ((String) cmbState.getSelectedItem()) {
					case "Watching": state = State.WATCHING; break;
					case "Finished": state = State.FINISHED; break;
					case "Dropped": state = State.DROPPED; break;
					case "Stalled": state = State.STALLED; break;
					case "Planning to watch": state = State.PLANNING_TO_WATCH; break;
					default: state = State.UPCOMMING; break;					
				}
				
				switch ((String) cmbType.getSelectedItem()) {
					case "TV": type = Types.TV; break;
					case "OAV": type = Types.OVA; break;
					case "ONA": type = Types.ONA; break;
					case "Special": type = Types.SPECIAL; break;
					default: type = Types.MOVIE; break;								
				}
				
				//Add the anime		
				/*if (cmbYear.getSelectedIndex() == 0) {
					Main.addAnime(new Anime(tfName.getText(), tfAltName.getText(), tfLink.getText(), (String) cmbAirDay.getSelectedItem(), fpDesription.getText(), type, 
											lblPrequelData.getText(), lblSequelData.getText(), state, Integer.parseInt(tfEpisodeData.getText()), 
											Integer.parseInt(tfTotEpisodesData.getText()), Integer.parseInt(tfEpisodeTime.getText()), cmbDay.getSelectedIndex(), (String) cmbMonth.getSelectedItem(), 
											0, Double.parseDouble(lblRatingData.getText()), pictureFile, chkbOngoing.isSelected()));
				} else {
					Main.addAnime(new Anime(tfName.getText(), tfAltName.getText(), tfLink.getText(), (String) cmbAirDay.getSelectedItem(), fpDesription.getText(), type, 
											lblPrequelData.getText(), lblSequelData.getText(), state, Integer.parseInt(tfEpisodeData.getText()), 
											Integer.parseInt(tfTotEpisodesData.getText()), Integer.parseInt(tfEpisodeTime.getText()), cmbDay.getSelectedIndex(), (String) cmbMonth.getSelectedItem(), 
											Integer.parseInt((String) cmbYear.getSelectedItem()), Double.parseDouble(lblRatingData.getText()), pictureFile, chkbOngoing.isSelected()));
				}*/
					
				//Update prequel/sequel dependencies
				/*if (!lblPrequelData.getText().equals("...")) {
					Main.getAnime(lblPrequelData.getText()).setSequel(tfName.getText());					
				}
				
				if (!lblSequelData.getText().equals("...")) {
					Main.getAnime(lblSequelData.getText()).setPrequel(tfName.getText());					
				}				
				
				Main.checkSearchState();
				Main.ensureFocus(tfName.getText());
				dispose();	*/
			}
		});
		pnlButtons.add(btnCreate);
		
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {							
				dispose();
			}
		});
		pnlButtons.add(btnCancel);
		
		lblPicture = new JLabel("Picture");
		lblPicture.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
		lblPicture.setOpaque(true);
		lblPicture.setForeground(Color.WHITE);
		lblPicture.setBackground(new Color(0, 102, 153));
		lblPicture.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPicture.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(10)
					.addComponent(lblPicture, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(tfName, GroupLayout.DEFAULT_SIZE, 664, Short.MAX_VALUE)
					.addGap(20))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(18)
					.addComponent(picture, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE)
					.addGap(20)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(tfAltName, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
						.addComponent(lblDescription, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 643, Short.MAX_VALUE))
					.addGap(31))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(14)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 903, Short.MAX_VALUE)
						.addComponent(lblInformation, GroupLayout.DEFAULT_SIZE, 903, Short.MAX_VALUE)
						.addComponent(pnlButtons, GroupLayout.DEFAULT_SIZE, 903, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(11)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblPicture, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfName, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(picture, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(tfAltName, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(lblDescription, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addGap(6)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)))
					.addGap(5)
					.addComponent(lblInformation, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 367, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlButtons, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(4))
		);
		panel.setLayout(gl_panel);
		
		setVisible(true);
		setLocationRelativeTo(null);
		getContentPane().requestFocus();
		
		startSate = false;
	}
	
	private void newPS(final String state, final JLabel label) {
		if (psSelector != null) {
			psSelector.dispose();
		}
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					psSelector = new PSSelector(state, label);				
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void updateColours(String state) {
		if (state.equals("Watching")) {
			tfName.setBackground(new Color(0, 102, 153));
			tfAltName.setBackground(new Color(0, 102, 153));
			lblPicture.setBackground(new Color(0, 102, 153));
			lblDescription.setBackground(new Color(100, 149, 237));
			lblInformation.setBackground(new Color(100, 149, 237));			
		} else if (state.equals("Finished")) {		
			tfName.setBackground(new Color(0, 153, 0));
			tfAltName.setBackground(new Color(0, 153, 0));
			lblPicture.setBackground(new Color(0, 153, 0));
			lblDescription.setBackground(new Color(51, 204, 0));
			lblInformation.setBackground(new Color(51, 204, 0));		
		} else if (state.equals("Dropped")) {
			tfName.setBackground(new Color(204, 0, 0));
			tfAltName.setBackground(new Color(204, 0, 0));
			lblPicture.setBackground(new Color(204, 0, 0));
			lblDescription.setBackground(new Color(255, 0, 0));
			lblInformation.setBackground(new Color(255, 0, 0));			
		} else if (state.equals("Stalled")) {
			tfName.setBackground(new Color(255, 51, 0));
			tfAltName.setBackground(new Color(255, 51, 0));
			lblPicture.setBackground(new Color(255, 51, 0));
			lblDescription.setBackground(new Color(255, 102, 0));
			lblInformation.setBackground(new Color(255, 102, 0));
		} else if (state.equals("Planning to watch")) {
			tfName.setBackground(Color.GRAY);
			tfAltName.setBackground(Color.GRAY);
			lblPicture.setBackground(Color.GRAY);
			lblDescription.setBackground(Color.LIGHT_GRAY);			
			lblInformation.setBackground(Color.LIGHT_GRAY);		
		} else {
			tfName.setBackground(new Color(153, 0, 153));
			tfAltName.setBackground(new Color(153, 0, 153));
			lblPicture.setBackground(new Color(153, 0, 153));
			lblDescription.setBackground(new Color(153, 0, 255));
			lblInformation.setBackground(new Color(153, 0, 255));	
		}
	}
	
	public void updatePS(boolean reset, String state, String name) {
		if (state.equals("Prequel")) {
			if (reset) {
				lblPrequelData.setText("...");
			} else {
				lblPrequelData.setText(name);
			}			
		} else {
			if (reset) {
				lblSequelData.setText("...");
			} else { 
				lblSequelData.setText(name);
			}
		}
	}
}
