package aor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import aor.main.Main;
import aor.ui.renderers.BoldListRenderer;

/**
 * @author Asmicor
 *
 */
public class PSSelector extends JDialog {
	
	private static final long serialVersionUID = 7174700542637842089L;
	
	private ArrayList<String> animeStrings;	
	
	private JLabel lblCurrentSelectedAnime;
	private JList<String> list;		
	
	private int numEntries = 0;
	
	private boolean safegaurd = false;
	
	private ArrayList<String> searchStrings;	
	private JTextField tfSearchBar;

	/**
	 * Create the dialog.
	 */
	public PSSelector(final String state, final JLabel label) {		
		setType(Type.UTILITY);				
		setTitle("Anime Organizer - " + state + " Selector");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 652, 518);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBorder(null);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setBorder(null);
		scrollPane.setViewportView(panel);
		
		JPanel pnlList = new JPanel();
		pnlList.setBackground(Color.WHITE);
		pnlList.setBorder(null);
		pnlList.setLayout(new BorderLayout(0, 0));
		
		lblCurrentSelectedAnime = new JLabel("...");
		lblCurrentSelectedAnime.setOpaque(true);
		lblCurrentSelectedAnime.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentSelectedAnime.setForeground(Color.WHITE);
		lblCurrentSelectedAnime.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblCurrentSelectedAnime.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		lblCurrentSelectedAnime.setBackground(new Color(0, 102, 153));
		
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBorder(null);
		pnlButtons.setBackground(Color.WHITE);
		pnlButtons.setLayout(new GridLayout(0, 3, 0, 0));
		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {				
				label.setText(lblCurrentSelectedAnime.getText());
				dispose();
			}
		});
		btnSelect.setToolTipText("");
		pnlButtons.add(btnSelect);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		pnlButtons.add(btnCancel);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {	
				label.setText("...");
				dispose();
			}
		});
		pnlButtons.add(btnReset);
		
		tfSearchBar = new JTextField();
		tfSearchBar.addKeyListener(new KeyAdapter() {			
			@Override
			public void keyReleased(KeyEvent arg0) {
				if (tfSearchBar.getText().equals("Search...")) {
					safegaurd = true;
					list.setListData(animeStrings.toArray(new String[animeStrings.size()]));				
					safegaurd = false;					
				} else {
					String search = tfSearchBar.getText().toUpperCase();				
					searchStrings = new ArrayList<String>();			
					
					for (int i = 0; i < numEntries; i = i + 1) {
						if (Main.getAnimeArray()[i].getName().toUpperCase().contains(search) || Main.getAnimeArray()[i].getAltName().toUpperCase().contains(search)) {
							searchStrings.add(Main.getAnimeArray()[i].getName());									
						}					
					}
					
					safegaurd = true;
					list.setListData(searchStrings.toArray(new String[searchStrings.size()]));						
					safegaurd = false;		
				}				
			}
		});
		tfSearchBar.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if (tfSearchBar.getText().equals("Search...")) {
					tfSearchBar.setText("");
				}
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				if (tfSearchBar.getText().equals("")) {
					tfSearchBar.setText("Search...");
				}
			}
		});
		tfSearchBar.setForeground(Color.DARK_GRAY);
		tfSearchBar.setText("Search...");
		tfSearchBar.setColumns(10);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(null);
		pnlList.add(scrollPane_1, BorderLayout.CENTER);
		
		list = new JList<String>();
		list.setCellRenderer(new BoldListRenderer());
		list.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				if (safegaurd == false && list.getSelectedValue() != null) {					
					lblCurrentSelectedAnime.setText(list.getSelectedValue());
				}					
			}
		});
		scrollPane_1.setViewportView(list);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(10)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCurrentSelectedAnime, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
							.addGap(10))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(pnlList, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
							.addGap(10))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(pnlButtons, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
							.addGap(10))))
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(tfSearchBar, GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(5)
					.addComponent(lblCurrentSelectedAnime, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(pnlList, GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
					.addGap(7)
					.addComponent(tfSearchBar, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(pnlButtons, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
					.addGap(4))
		);
		panel.setLayout(gl_panel);
		
		animeStrings = new ArrayList<String>();
		for (int i = 0; i < Main.getAnimeArray().length; i = i + 1) {
			if (Main.getAnimeArray()[i] != null) {
				animeStrings.add(Main.getAnimeArray()[i].getName());				
				numEntries = numEntries + 1;
			} else {
				break;			
			}
		}
		
		list.setListData(animeStrings.toArray(new String[animeStrings.size()]));
		
		setVisible(true);
		setLocationRelativeTo(null);		
	}	
}
