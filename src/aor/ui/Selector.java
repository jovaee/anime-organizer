/**
 * 
 */
package aor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListModel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import aor.graphics.Theme;
import aor.main.Main;
import aor.objects.Anime;
import aor.objects.enums.Mode;
import aor.objects.enums.State;
import aor.ui.renderers.BoldListRenderer;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.FlowLayout;

/**
 * @author Asmicor
 *
 */
public class Selector extends JFrame {

    private static final long serialVersionUID = -4519528818702331597L;

    private ArrayList<String> animeStrings = new ArrayList<String>();
    
    private int numEntries = 0;
    private int totalTimeViewed = 0;
    
    /*
     * This is used to prevent action events from firing. 
     * Set to true when any events should not fire. Remember to set to false again
     * This is used to avoid Null pointers when values are changed and there is no data to read from.
     */
    private boolean safegaurd = true;

    private JPanel contentPane;

    private JList<String> jList;
    private String lastSelectedAnime = "";

    private JLabel lblAirDay;

    private JLabel lblAirDayData;
    private JLabel lblAltName;
    private JLabel lblEpisodeData;
    private JLabel lblInfo;
    private JLabel lblName;
    private JLabel lblNextEpisodeData;
    private JLabel lblStateData;
    private JLabel lblTotalTimeViewedData;
    private JLabel lblUpcomDay;
    private JLabel lblUpcomDayData;

    private ArrayList<String> searchStrings = new ArrayList<String>();
    private JTextField tfSearchBar;
    private JToggleButton tglbtnStateSwitch;

    private JLabel picture;
    private JSeparator separator;
    private JPopupMenu popupMenu_1;
    private JMenuItem mntmNewMenuItem_1;
    private JMenuItem mntmNewMenuItem_2;
    private JSeparator separator_1;
    private JPopupMenu popupMenu;
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem_1;
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem_2;
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem_3;
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem_4;
    private JMenuItem mntmNewMenuItem;
    private JSeparator separator_2;
    private JRadioButtonMenuItem rdbtnmntmNewRadioItem_5;
    private JLabel lblPopupAnimeName;
    private JRadioButtonMenuItem rdbtnmntmPlanningToWatch;

    /**
     * Create the frame.
     */
    public Selector(Anime[] anime, int entries) {
    	setTitle("Anime Organizer");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setBounds(100, 100, 1080, 695);
	contentPane = new JPanel();
	contentPane.setInheritsPopupMenu(true);
	contentPane.setBorder(null);
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);

	separator = new JSeparator();

	JPanel panel = new JPanel();
	panel.setBorder(null);
	contentPane.add(panel, BorderLayout.CENTER);
	panel.setInheritsPopupMenu(true);
	panel.setBackground(Color.WHITE);

	JPanel pnlList = new JPanel();
	pnlList.setInheritsPopupMenu(true);
	pnlList.setBorder(null);

	lblName = new JLabel("Please select an anime...");
	lblName.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
	lblName.setOpaque(true);
	lblName.setBackground(new Color(0, 102, 153));
	lblName.setForeground(Color.WHITE);
	lblName.setHorizontalAlignment(SwingConstants.CENTER);
	lblName.setFont(new Font("Tahoma", Font.BOLD, 15));
	pnlList.setLayout(new BorderLayout(0, 0));

	JScrollPane scrollPane_1 = new JScrollPane();
	scrollPane_1.setInheritsPopupMenu(true);
	scrollPane_1.setBorder(null);
	pnlList.add(scrollPane_1, BorderLayout.CENTER);

	jList = new JList<String>();
	jList.setInheritsPopupMenu(true);
	jList.setCellRenderer(new BoldListRenderer());
	jList.setBorder(null);
	jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	jList.addListSelectionListener(new ListSelectionListener() {
	    @Override
	    public void valueChanged(ListSelectionEvent arg0) {
		// Check that a valid anime is selected and that the same entry was not clicked on.
		if (safegaurd == false && jList.getSelectedValue() != null && !lastSelectedAnime.equals(jList.getSelectedValue())) {
		    safegaurd = true;
		    
		    Anime currentAnime = Main.getAnime(jList.getSelectedValue());
		    lastSelectedAnime = currentAnime.getName();
		    safegaurd = false;

		    // Change label text
		    lblName.setText(currentAnime.getName());
		    lblAltName.setText(currentAnime.getAltName());
		    lblPopupAnimeName.setText("Selected: " + currentAnime.getName());

		    // Increment episode count check
		    if ((currentAnime.getTotalEpisodes() == 0 || currentAnime.getCurrentEpisode() < currentAnime.getTotalEpisodes()) && currentAnime.getState() != State.UPCOMMING && currentAnime.getState() != State.PLANNING_TO_WATCH) {
			mntmNewMenuItem_1.setEnabled(true);
		    } else {
			mntmNewMenuItem_1.setEnabled(false);
		    }

		    // Decrement episode count check
		    if (currentAnime.getCurrentEpisode() <= 0 || currentAnime.getState() == State.FINISHED) {
			mntmNewMenuItem_2.setEnabled(false);
		    } else {
			mntmNewMenuItem_2.setEnabled(true);
		    }

		    // Update the info panel with necessary info
		    updateInfoPanel(currentAnime);

		    // Change label color according to the state of the anime
		    updateLabelColor(currentAnime.getState());
		} else if (jList.getSelectedIndex() < 0 || jList.getSelectedIndex() >= animeStrings.size() - 1) {
		    mntmNewMenuItem_2.setEnabled(false);
		    mntmNewMenuItem_1.setEnabled(false);
		    lblPopupAnimeName.setText("...");
		}
	    }
	});
	scrollPane_1.setViewportView(jList);

	popupMenu_1 = new JPopupMenu();
	addPopup(jList, popupMenu_1);

	lblPopupAnimeName = new JLabel("...");
	lblPopupAnimeName.setHorizontalAlignment(SwingConstants.CENTER);
	lblPopupAnimeName.setFont(new Font("Tahoma", Font.BOLD, 12));
	popupMenu_1.add(lblPopupAnimeName);

	separator_1 = new JSeparator();
	popupMenu_1.add(separator_1);

	mntmNewMenuItem_1 = new JMenuItem("Increment Episode (+)");
	mntmNewMenuItem_1.setEnabled(false);
	mntmNewMenuItem_1.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if (jList.getSelectedValue() != null) {
		    int index = jList.getSelectedIndex();
		    Anime currentAnime = Main.getAnime(jList.getSelectedValue());

		    if ((currentAnime.getTotalEpisodes() == 0
			    || currentAnime.getCurrentEpisode() < currentAnime.getTotalEpisodes())
			    && currentAnime.getState() != State.UPCOMMING
			    && currentAnime.getState() != State.PLANNING_TO_WATCH) {
			currentAnime.setCurrentEpisode(currentAnime.getCurrentEpisode() + 1);

			if (currentAnime.getCurrentEpisode() == currentAnime.getTotalEpisodes()
				&& currentAnime.getTotalEpisodes() != 0) {
			    currentAnime.setState(State.FINISHED);

			    Main.updateEntry(currentAnime.getName(), currentAnime);
			    Main.newViewer(currentAnime.getName(), Mode.UPDATE);
			    Main.newError("Please Rate The Anime");
			} else {
			    Main.updateEntry(currentAnime.getName(), currentAnime);
			}

			updateInfoPanel(currentAnime);
		    }

		    if (currentAnime.getTotalEpisodes() != 0
			    || currentAnime.getCurrentEpisode() >= currentAnime.getTotalEpisodes()) {
			mntmNewMenuItem_1.setEnabled(false);
		    } else {
			mntmNewMenuItem_1.setEnabled(true);
		    }

		    // Decrement episode count check
		    if (currentAnime.getCurrentEpisode() <= 0) {
			mntmNewMenuItem_2.setEnabled(false);
		    } else {
			mntmNewMenuItem_2.setEnabled(true);
		    }

		    jList.setSelectedIndex(index);
		}
	    }
	});
	popupMenu_1.add(mntmNewMenuItem_1);

	mntmNewMenuItem_2 = new JMenuItem("Decrement Episode (-)");
	mntmNewMenuItem_2.setEnabled(false);
	mntmNewMenuItem_2.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if (jList.getSelectedValue() != null) {
		    int index = jList.getSelectedIndex();
		    Anime currentAnime = Main.getAnime(jList.getSelectedValue());

		    if (currentAnime.getCurrentEpisode() > 0 && currentAnime.getState() != State.FINISHED
			    && currentAnime.getState() != State.UPCOMMING
			    && currentAnime.getState() != State.PLANNING_TO_WATCH) {
			currentAnime.setCurrentEpisode(currentAnime.getCurrentEpisode() - 1);
			Main.updateEntry(currentAnime.getName(), currentAnime);
			updateInfoPanel(currentAnime);
		    }

		    if (currentAnime.getTotalEpisodes() <= 0 || currentAnime.getState() == State.FINISHED) {
			mntmNewMenuItem_1.setEnabled(false);
		    } else {
			mntmNewMenuItem_1.setEnabled(true);
		    }

		    // Decrement episode count check
		    if (currentAnime.getCurrentEpisode() <= 0 || currentAnime.getState() == State.FINISHED) {
			mntmNewMenuItem_2.setEnabled(false);
		    } else {
			mntmNewMenuItem_2.setEnabled(true);
		    }

		    jList.setSelectedIndex(index);
		}
	    }
	});
	popupMenu_1.add(mntmNewMenuItem_2);

	lblInfo = new JLabel("Quick Info:");
	lblInfo.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	lblInfo.setOpaque(true);
	lblInfo.setHorizontalAlignment(SwingConstants.CENTER);
	lblInfo.setBackground(new Color(100, 149, 237));
	lblInfo.setFont(new Font("Tahoma", Font.BOLD, 13));

	JPanel pnlInfo = new JPanel();
	pnlInfo.setInheritsPopupMenu(true);
	pnlInfo.setBackground(Color.WHITE);

	lblAltName = new JLabel("...");
	lblAltName.setOpaque(true);
	lblAltName.setHorizontalAlignment(SwingConstants.CENTER);
	lblAltName.setForeground(Color.WHITE);
	lblAltName.setFont(new Font("Tahoma", Font.BOLD, 13));
	lblAltName.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
	lblAltName.setBackground(new Color(0, 102, 153));

	JLabel lblEpisode = new JLabel("Episode:");
	lblEpisode.setHorizontalAlignment(SwingConstants.LEFT);
	lblEpisode.setFont(new Font("Tahoma", Font.BOLD, 12));

	JLabel lblNextEpisode = new JLabel("Next Episode:");
	lblNextEpisode.setHorizontalAlignment(SwingConstants.LEFT);
	lblNextEpisode.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblAirDay = new JLabel("Air Day:");
	lblAirDay.setVisible(false);
	lblAirDay.setHorizontalAlignment(SwingConstants.LEFT);
	lblAirDay.setFont(new Font("Tahoma", Font.BOLD, 12));

	JLabel lblState = new JLabel("State:");
	lblState.setHorizontalAlignment(SwingConstants.LEFT);
	lblState.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblUpcomDay = new JLabel("Upcomming Day:");
	lblUpcomDay.setHorizontalAlignment(SwingConstants.LEFT);
	lblUpcomDay.setVisible(false);
	lblUpcomDay.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblEpisodeData = new JLabel("...");
	lblEpisodeData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblEpisodeData.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblNextEpisodeData = new JLabel("...");
	lblNextEpisodeData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblNextEpisodeData.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblAirDayData = new JLabel("...");
	lblAirDayData.setVisible(false);
	lblAirDayData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblAirDayData.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblStateData = new JLabel("...");
	lblStateData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblStateData.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblUpcomDayData = new JLabel("...");
	lblUpcomDayData.setVisible(false);
	lblUpcomDayData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblUpcomDayData.setFont(new Font("Tahoma", Font.BOLD, 12));

	JLabel lblTotalTimeViewed = new JLabel("Total Time Viewed");
	lblTotalTimeViewed.setHorizontalAlignment(SwingConstants.RIGHT);
	lblTotalTimeViewed.setFont(new Font("Tahoma", Font.BOLD, 12));

	lblTotalTimeViewedData = new JLabel("...");
	lblTotalTimeViewed.setLabelFor(lblTotalTimeViewedData);
	lblTotalTimeViewedData.setHorizontalAlignment(SwingConstants.RIGHT);
	lblTotalTimeViewedData.setFont(new Font("Tahoma", Font.BOLD, 12));

	tfSearchBar = new JTextField();
	tfSearchBar.setBackground(Color.WHITE);
	tfSearchBar.addKeyListener(new KeyAdapter() {
	    @Override
	    public void keyReleased(KeyEvent arg0) {
		if (tglbtnStateSwitch.isSelected()) {
		    searchStateWise();
		} else {
		    searchNameWise();
		}
	    }
	});

	tfSearchBar.setForeground(Color.DARK_GRAY);
	tfSearchBar.setToolTipText("When searching for a certain anime or state note that the whole word does not have to be typed in order to get results");
	tfSearchBar.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusGained(FocusEvent arg0) {
		if (tfSearchBar.getText().equals("Search...")) {
		    tfSearchBar.setText("");
		}
	    }

	    @Override
	    public void focusLost(FocusEvent arg0) {
		if (tfSearchBar.getText().equals("")) {
		    resetSearchState();
		}
	    }
	});
	tfSearchBar.setText("Search...");
	tfSearchBar.setColumns(10);

	tglbtnStateSwitch = new JToggleButton("Name Search");
	tglbtnStateSwitch.setToolTipText("Searches the name of the anime");
	tglbtnStateSwitch.addItemListener(new ItemListener() {
	    @Override
	    public void itemStateChanged(ItemEvent arg0) {
		if (tglbtnStateSwitch.isSelected()) {
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();
		} else {
		    tfSearchBar.setBackground(Color.WHITE);
		    tglbtnStateSwitch.setText("Name Search");
		    tglbtnStateSwitch.setToolTipText("Searches the name of the anime");
		    searchNameWise();
		}

		tfSearchBar.requestFocus();
	    }
	});

	JPanel pnlButtons = new JPanel();
	pnlButtons.setBackground(Color.WHITE);

	popupMenu = new JPopupMenu();
	addPopup(tfSearchBar, popupMenu);

	rdbtnmntmNewRadioItem = new JRadioButtonMenuItem("Airing");
	rdbtnmntmNewRadioItem.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem.isSelected()) {
		    tfSearchBar.setText("Airing");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});

	mntmNewMenuItem = new JMenuItem("Clear");
	mntmNewMenuItem.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		tfSearchBar.setText("Search...");

		tglbtnStateSwitch.setSelected(false);
		tfSearchBar.setBackground(Color.WHITE);

		tfSearchBar.requestFocus();

		buttonGroup.clearSelection();
	    }
	});
	popupMenu.add(mntmNewMenuItem);

	separator_2 = new JSeparator();
	popupMenu.add(separator_2);
	buttonGroup.add(rdbtnmntmNewRadioItem);
	popupMenu.add(rdbtnmntmNewRadioItem);

	rdbtnmntmNewRadioItem_4 = new JRadioButtonMenuItem("Watching");
	rdbtnmntmNewRadioItem_4.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem_4.isSelected()) {
		    tfSearchBar.setText("Watching");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});

	rdbtnmntmNewRadioItem_5 = new JRadioButtonMenuItem("Released");
	rdbtnmntmNewRadioItem_5.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem_5.isSelected()) {
		    tfSearchBar.setText("Released");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});
	buttonGroup.add(rdbtnmntmNewRadioItem_5);
	popupMenu.add(rdbtnmntmNewRadioItem_5);
	buttonGroup.add(rdbtnmntmNewRadioItem_4);
	popupMenu.add(rdbtnmntmNewRadioItem_4);

	rdbtnmntmNewRadioItem_3 = new JRadioButtonMenuItem("Finished");
	rdbtnmntmNewRadioItem_3.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem_3.isSelected()) {
		    tfSearchBar.setText("Finished");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});
	buttonGroup.add(rdbtnmntmNewRadioItem_3);
	popupMenu.add(rdbtnmntmNewRadioItem_3);

	rdbtnmntmNewRadioItem_2 = new JRadioButtonMenuItem("Stalled");
	rdbtnmntmNewRadioItem_2.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem_2.isSelected()) {
		    tfSearchBar.setText("Stalled");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});
	buttonGroup.add(rdbtnmntmNewRadioItem_2);
	popupMenu.add(rdbtnmntmNewRadioItem_2);

	rdbtnmntmNewRadioItem_1 = new JRadioButtonMenuItem("Upcomming");
	rdbtnmntmNewRadioItem_1.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmNewRadioItem_1.isSelected()) {
		    tfSearchBar.setText("Upcomming");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});

	rdbtnmntmPlanningToWatch = new JRadioButtonMenuItem("Planning to Watch");
	buttonGroup.add(rdbtnmntmPlanningToWatch);
	rdbtnmntmPlanningToWatch.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (rdbtnmntmPlanningToWatch.isSelected()) {
		    tfSearchBar.setText("Planning");

		    tglbtnStateSwitch.setSelected(true);
		    tfSearchBar.setBackground(SystemColor.inactiveCaptionBorder);
		    tglbtnStateSwitch.setText("State Search");
		    tglbtnStateSwitch.setToolTipText("Searches the state of the anime -- Watching, Planning, Finished, Dropped, Stalled, Upcomming, Released and Airing");
		    searchStateWise();

		    tfSearchBar.requestFocus();
		}
	    }
	});
	popupMenu.add(rdbtnmntmPlanningToWatch);
	buttonGroup.add(rdbtnmntmNewRadioItem_1);
	popupMenu.add(rdbtnmntmNewRadioItem_1);
	pnlButtons.setLayout(new GridLayout(0, 4, 0, 0));

	JButton btnSelect = new JButton("Select");
	pnlButtons.add(btnSelect);

	JButton btnUpdate = new JButton("Update");
	pnlButtons.add(btnUpdate);

	JButton btnCreate = new JButton("Create");
	pnlButtons.add(btnCreate);

	JButton btnDelete = new JButton("Delete");
	pnlButtons.add(btnDelete);
	btnDelete.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		/*
		 * if (!lblName.getText().equals("Please select an anime...")) {
		 * if
		 * (!Main.getAnime(jList.getSelectedValue()).getPrequel().equals
		 * ("...")) {
		 * Main.getAnime(Main.getAnime(jList.getSelectedValue()).
		 * getPrequel()).setSequel("..."); }
		 * 
		 * if
		 * (!Main.getAnime(jList.getSelectedValue()).getSequel().equals(
		 * "...")) {
		 * Main.getAnime(Main.getAnime(jList.getSelectedValue()).
		 * getSequel()).setPrequel("..."); }
		 * 
		 * Main.deleteAnime(jList.getSelectedValue()); resetInfoPanel();
		 * } else { Main.newError(
		 * "Please select an anime from the list first"); }
		 */
	    }
	});
	btnCreate.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		// Main.newCreator();
		Main.newViewer("New Anime", Mode.CREATE);
	    }
	});
	btnUpdate.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		if (!lblName.getText().equals("Please select an anime...")) {
		    Main.newViewer(lblName.getText(), Mode.UPDATE);
		} else {
		    Main.newError("Please select an anime from the list first");
		}
	    }
	});
	btnSelect.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent arg0) {
		if (!lblName.getText().equals("Please select an anime...")) {
		    Main.newViewer(lblName.getText(), Mode.VIEW);
		} else {
		    Main.newError("Please select an anime from the list first");
		}
	    }
	});
	GroupLayout gl_pnlInfo = new GroupLayout(pnlInfo);
	gl_pnlInfo
		.setHorizontalGroup(gl_pnlInfo.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_pnlInfo.createSequentialGroup().addGap(10)
				.addGroup(gl_pnlInfo.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_pnlInfo.createSequentialGroup()
						.addComponent(lblEpisode, GroupLayout.PREFERRED_SIZE, 58,
							GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 233, Short.MAX_VALUE)
						.addComponent(lblEpisodeData, GroupLayout.PREFERRED_SIZE, 122,
							GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
					.addGroup(gl_pnlInfo.createSequentialGroup()
						.addComponent(lblNextEpisode, GroupLayout.PREFERRED_SIZE, 95,
							GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 196, Short.MAX_VALUE)
						.addComponent(lblNextEpisodeData, GroupLayout.PREFERRED_SIZE, 122,
							GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
					.addGroup(gl_pnlInfo.createSequentialGroup()
						.addComponent(lblState, GroupLayout.PREFERRED_SIZE, 58,
							GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
						.addComponent(lblStateData, GroupLayout.PREFERRED_SIZE, 309,
							GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
					.addGroup(gl_pnlInfo.createSequentialGroup()
						.addComponent(lblAirDay, GroupLayout.PREFERRED_SIZE, 58,
							GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 192, Short.MAX_VALUE)
						.addComponent(lblAirDayData, GroupLayout.PREFERRED_SIZE, 163,
							GroupLayout.PREFERRED_SIZE)
						.addContainerGap())
					.addGroup(gl_pnlInfo.createSequentialGroup().addComponent(lblUpcomDay)
						.addPreferredGap(ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
						.addComponent(lblUpcomDayData, GroupLayout.PREFERRED_SIZE, 197,
							GroupLayout.PREFERRED_SIZE)
						.addContainerGap())))
			.addGroup(Alignment.TRAILING,
				gl_pnlInfo.createSequentialGroup().addContainerGap(312, Short.MAX_VALUE)
					.addComponent(lblTotalTimeViewed).addContainerGap())
			.addGroup(Alignment.TRAILING, gl_pnlInfo.createSequentialGroup().addContainerGap()
				.addComponent(lblTotalTimeViewedData, GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
				.addContainerGap()));
	gl_pnlInfo.setVerticalGroup(gl_pnlInfo.createParallelGroup(Alignment.LEADING).addGroup(gl_pnlInfo
		.createSequentialGroup().addGap(11)
		.addGroup(gl_pnlInfo.createParallelGroup(Alignment.BASELINE).addComponent(lblEpisode)
			.addComponent(lblEpisodeData))
		.addGap(15)
		.addGroup(gl_pnlInfo.createParallelGroup(Alignment.BASELINE).addComponent(lblNextEpisode)
			.addComponent(lblNextEpisodeData))
		.addGap(15)
		.addGroup(gl_pnlInfo.createParallelGroup(Alignment.BASELINE).addComponent(lblState)
			.addComponent(lblStateData))
		.addGap(15)
		.addGroup(gl_pnlInfo.createParallelGroup(Alignment.BASELINE).addComponent(lblAirDay)
			.addComponent(lblAirDayData))
		.addGap(15)
		.addGroup(gl_pnlInfo.createParallelGroup(Alignment.BASELINE).addComponent(lblUpcomDay)
			.addComponent(lblUpcomDayData))
		.addPreferredGap(ComponentPlacement.RELATED, 17, Short.MAX_VALUE).addComponent(lblTotalTimeViewed)
		.addPreferredGap(ComponentPlacement.RELATED)
		.addComponent(lblTotalTimeViewedData, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
		.addContainerGap()));
	pnlInfo.setLayout(gl_pnlInfo);
	
	JPanel panel_1 = new JPanel();
	panel_1.setBorder(null);
	panel_1.setBackground(Color.WHITE);
	GroupLayout gl_panel = new GroupLayout(panel);
	gl_panel.setHorizontalGroup(
		gl_panel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel.createSequentialGroup()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(10)
						.addComponent(lblName, GroupLayout.DEFAULT_SIZE, 1042, Short.MAX_VALUE)
						.addGap(1))
					.addGroup(gl_panel.createSequentialGroup()
						.addGap(20)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
									.addComponent(pnlList, GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE)
									.addGroup(gl_panel.createSequentialGroup()
										.addComponent(tfSearchBar, GroupLayout.DEFAULT_SIZE, 478, Short.MAX_VALUE)
										.addGap(7)
										.addComponent(tglbtnStateSwitch, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE))
									.addComponent(pnlButtons, GroupLayout.DEFAULT_SIZE, 588, Short.MAX_VALUE))
								.addGap(10)
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
									.addComponent(pnlInfo, GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
									.addGroup(gl_panel.createSequentialGroup()
										.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
											.addGroup(Alignment.LEADING, gl_panel.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE))
											.addComponent(lblInfo, GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE))
										.addGap(10))))
							.addGroup(gl_panel.createSequentialGroup()
								.addComponent(lblAltName, GroupLayout.DEFAULT_SIZE, 1023, Short.MAX_VALUE)
								.addGap(10)))))
				.addGap(11))
	);
	gl_panel.setVerticalGroup(
		gl_panel.createParallelGroup(Alignment.LEADING)
			.addGroup(gl_panel.createSequentialGroup()
				.addGap(10)
				.addComponent(lblName, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
				.addGap(5)
				.addComponent(lblAltName, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
				.addGap(6)
				.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(pnlList, GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(9)
								.addComponent(tfSearchBar, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
							.addGroup(gl_panel.createSequentialGroup()
								.addGap(7)
								.addComponent(tglbtnStateSwitch)))
						.addGap(7)
						.addComponent(pnlButtons, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel.createSequentialGroup()
						.addComponent(lblInfo, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(pnlInfo, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
						.addGap(1)))
				.addGap(4))
	);
		panel_1.setLayout(new GridLayout(1, 1, 0, 0));
	
		picture = new JLabel("");
		panel_1.add(picture);
		picture.setBorder(null);
		picture.setHorizontalAlignment(SwingConstants.CENTER);
	panel.setLayout(gl_panel);

	addAnimeString(anime, entries);
	safegaurd = false;

	setVisible(true);
	setLocationRelativeTo(null);
    }

    public void addAnimeString(Anime[] anime, int entries) {
	animeStrings.clear();
	totalTimeViewed = 0;

	for (int i = 0; i < entries; i = i + 1) {
	    animeStrings.add(anime[i].getName());
	    totalTimeViewed = totalTimeViewed + (anime[i].getEpisodeRuntime() * anime[i].getCurrentEpisode());
	}

	numEntries = entries;

	safegaurd = true;
	jList.setListData(animeStrings.toArray(new String[animeStrings.size()]));
	safegaurd = false;

	updateTimeViewed();
    }

    public void ensureFocus(int index) {
	jList.ensureIndexIsVisible(index);
    }

    public void listFocus() {
	jList.requestFocus();
    }

    public void removeAnimeString(Anime[] anime, int entries) {
	animeStrings.clear();

	for (int i = 0; i < entries; i = i + 1) {
	    animeStrings.add(anime[i].getName());
	    totalTimeViewed = totalTimeViewed + (anime[i].getEpisodeRuntime() * anime[i].getCurrentEpisode());
	}

	numEntries = entries;

	safegaurd = true;
	jList.setListData(animeStrings.toArray(new String[animeStrings.size()]));
	safegaurd = false;

	updateTimeViewed();
    }

    public void resetInfoPanel() {
	lblName.setText("Please select an anime...");
	lblAltName.setText("...");

	lblUpcomDay.setVisible(false);
	lblUpcomDayData.setVisible(false);

	lblEpisodeData.setText("...");
	lblNextEpisodeData.setText("...");

	lblStateData.setText("...");

	lblAirDay.setVisible(false);
	lblAirDayData.setVisible(false);

	updateLabelColor(State.WATCHING);
    }

    public void resetSearchState() {
	safegaurd = true;
	jList.setListData(animeStrings.toArray(new String[animeStrings.size()]));
	safegaurd = false;

	Main.setSearchState(0);

	tfSearchBar.setText("Search...");
    }

    public void searchNameWise() {
	if (tfSearchBar.getText().equals("Search...") || tfSearchBar.getText().equals("")) {
	    safegaurd = true;
	    jList.setListData(animeStrings.toArray(new String[animeStrings.size()]));
	    safegaurd = false;
	} else {
	    String search = tfSearchBar.getText().toUpperCase();
	    searchStrings.clear();

	    for (int i = 0; i < numEntries; i = i + 1) {
		if (Main.getAnimeArray()[i].getName().toUpperCase().contains(search) || Main.getAnimeArray()[i].getAltName().toUpperCase().contains(search)) {
		    searchStrings.add(Main.getAnimeArray()[i].getName());
		}
	    }

	    safegaurd = true;
	    jList.setListData(searchStrings.toArray(new String[searchStrings.size()]));
	    safegaurd = false;

	    Main.setSearchState(1);
	}
    }

    public void searchStateWise() {
	if (tfSearchBar.getText().equals("Search...") || tfSearchBar.getText().equals("")) {
	    safegaurd = true;
	    jList.setListData(animeStrings.toArray(new String[animeStrings.size()]));
	    safegaurd = false;
	} else {
	    String search = tfSearchBar.getText().toUpperCase();
	    searchStrings.clear();

	    if ("AIRING".contains(search)) {
		for (int i = 0; i < numEntries; i = i + 1) {
		    if (Main.getAnimeArray()[i].isOngoing()) {
			searchStrings.add(Main.getAnimeArray()[i].getName());
		    }
		}

	    } else if ("RELEASED".contains(search)) {
		int day = 0;

		switch (Calendar.getInstance().get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
		    day = 1;
		    break;
		case Calendar.TUESDAY:
		    day = 2;
		    break;
		case Calendar.WEDNESDAY:
		    day = 3;
		    break;
		case Calendar.THURSDAY:
		    day = 4;
		    break;
		case Calendar.FRIDAY:
		    day = 5;
		    break;
		case Calendar.SATURDAY:
		    day = 6;
		    break;
		case Calendar.SUNDAY:
		    day = 7;
		    break;

		default:
		    day = 0;
		    break;
		}

		for (int i = 0; i < numEntries; i = i + 1) {
		    if (Main.getAnimeArray()[i].getAirDay() == day) {
			searchStrings.add(Main.getAnimeArray()[i].getName());
		    }
		}
	    } else if ("PLANNING".contains(search)) {
		for (int i = 0; i < numEntries; i = i + 1) {
		    if (Main.getAnimeArray()[i].getState() == State.PLANNING_TO_WATCH
			    && Main.getAnimeArray()[i].getState().toString().toUpperCase().contains(search)) {
			searchStrings.add(Main.getAnimeArray()[i].getName());
		    }
		}
	    } else {
		for (int i = 0; i < numEntries; i = i + 1) {
		    if (Main.getAnimeArray()[i].getState().toString().toUpperCase().contains(search)
			    && Main.getAnimeArray()[i].getState() != State.PLANNING_TO_WATCH) {
			searchStrings.add(Main.getAnimeArray()[i].getName());
		    }
		}
	    }

	    safegaurd = true;
	    jList.setListData(searchStrings.toArray(new String[searchStrings.size()]));
	    safegaurd = false;

	    Main.setSearchState(2);
	}
    }

    public void updateCurrentAnime(Anime anime) {
	updateInfoPanel(anime);
    }

    public void updateInfoPanel(Anime currentAnime) {
	lblName.setText(currentAnime.getName());
	lblAltName.setText(currentAnime.getAltName());

	if (currentAnime.getState() == State.UPCOMMING) {
	    lblUpcomDay.setVisible(true);
	    lblUpcomDayData.setVisible(true);

	    if (currentAnime.getStartAirDay() == 0 && currentAnime.getStartAirMonth() == 0 && currentAnime.getStartAirYear() == 0) {
		lblUpcomDayData.setText("...");
	    } else {
		lblUpcomDayData.setText("");

		if (currentAnime.getStartAirDay() != 0) {
		    lblUpcomDayData.setText(Integer.toString(currentAnime.getStartAirDay()));
		}

		if (currentAnime.getStartAirMonth() != 0) {
		    lblUpcomDayData.setText(
			    lblUpcomDayData.getText() + " " + Anime.parseMonth(currentAnime.getStartAirMonth()));
		}

		if (currentAnime.getStartAirYear() != 0) {
		    lblUpcomDayData.setText(
			    lblUpcomDayData.getText() + " " + Integer.toString(currentAnime.getStartAirYear()));
		}
	    }

	} else {
	    lblUpcomDay.setVisible(false);
	    lblUpcomDayData.setVisible(false);
	}

	if (currentAnime.getTotalEpisodes() == 0) {
	    lblEpisodeData.setText(Integer.toString(currentAnime.getCurrentEpisode()) + " / -");
	} else {
	    lblEpisodeData.setText(Integer.toString(currentAnime.getCurrentEpisode()) + " / "
		    + (Integer.toString(currentAnime.getTotalEpisodes())));
	}

	if (currentAnime.getState() != State.UPCOMMING && currentAnime.getState() != State.DROPPED
		&& currentAnime.getState() != State.FINISHED) {
	    lblNextEpisodeData.setText(Integer.toString(currentAnime.getCurrentEpisode() + 1));
	} else {
	    lblNextEpisodeData.setText("...");
	}

	if (currentAnime.isOngoing()) {
	    lblStateData.setText(currentAnime.getState().toString() + " and is currently airing");
	    lblAirDay.setVisible(true);
	    lblAirDayData.setVisible(true);
	    lblAirDayData.setText(Anime.parseDay(currentAnime.getAirDay()));
	} else {
	    lblStateData.setText(currentAnime.getState().toString());
	    lblAirDay.setVisible(false);
	    lblAirDayData.setVisible(false);
	}

	if (currentAnime.getAnimeArt().equals("...")) {
	    picture.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(Anime.ANIME_ART_EMPTY_VALUE)).getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_SMOOTH)));
	} else {
	    picture.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\Pictures\\" + currentAnime.getAnimeArt()).getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_SMOOTH)));
	}

	updateLabelColor(currentAnime.getState());
    }

    private void updateLabelColor(State state) {
	if (state == State.WATCHING) {
	    lblName.setBackground(Theme.WAITING_BASE_COLOR);
	    lblAltName.setBackground(Theme.WAITING_BASE_COLOR);
	    lblInfo.setBackground(Theme.WAITING_ACCENT_COLOR);
	} else if (state == State.FINISHED) {
	    lblName.setBackground(Theme.FINISHED_BASE_COLOR);
	    lblAltName.setBackground(Theme.FINISHED_BASE_COLOR);
	    lblInfo.setBackground(Theme.FINISHED_ACCENT_COLOR);
	} else if (state == State.DROPPED) {
	    lblName.setBackground(Theme.DROPPED_BASE_COLOR);
	    lblAltName.setBackground(Theme.DROPPED_BASE_COLOR);
	    lblInfo.setBackground(Theme.DROPPED_ACCENT_COLOR);
	} else if (state == State.STALLED) {
	    lblName.setBackground(Theme.STALLED_BASE_COLOR);
	    lblAltName.setBackground(Theme.STALLED_BASE_COLOR);
	    lblInfo.setBackground(Theme.STALLED_ACCENT_COLOR);
	} else if (state == State.PLANNING_TO_WATCH) {
	    lblName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    lblAltName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    lblInfo.setBackground(Theme.UPCOMMING_ACCENT_COLOR);
	} else {
	    lblName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    lblAltName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    lblInfo.setBackground(Theme.UPCOMMING_ACCENT_COLOR);
	}
    }

    private void updateTimeViewed() {
	int years, weeks, days, hours;
	years = weeks = days = hours = 0;

	lblTotalTimeViewedData.setText("");

	if (totalTimeViewed / 524160 >= 1) {
	    years = totalTimeViewed / 524160;
	    lblTotalTimeViewedData.setText(Integer.toString(years) + " Year(s), ");
	}

	totalTimeViewed = totalTimeViewed - (524160 * years);

	if (totalTimeViewed / 10080 >= 1) {
	    weeks = totalTimeViewed / 10080;
	    lblTotalTimeViewedData.setText(lblTotalTimeViewedData.getText() + Integer.toString(weeks) + " Week(s), ");
	}

	totalTimeViewed = totalTimeViewed - (10080 * weeks);

	if (totalTimeViewed / 1440 >= 1) {
	    days = totalTimeViewed / 1440;
	    lblTotalTimeViewedData.setText(lblTotalTimeViewedData.getText() + Integer.toString(days) + " Day(s), ");
	}

	totalTimeViewed = totalTimeViewed - (1440 * days);

	if (totalTimeViewed / 60 >= 1) {
	    hours = totalTimeViewed / 60;
	    lblTotalTimeViewedData.setText(lblTotalTimeViewedData.getText() + Integer.toString(hours) + " Hour(s), ");
	}

	totalTimeViewed = totalTimeViewed - (60 * hours);

	if (totalTimeViewed != 0) {
	    lblTotalTimeViewedData
		    .setText(lblTotalTimeViewedData.getText() + Integer.toString(totalTimeViewed) + " Minute(s)");
	}
    }

    private void addPopup(Component component, final JPopupMenu popup) {
	component.addMouseListener(new MouseAdapter() {
	    public void mousePressed(MouseEvent e) {
		if (e.isPopupTrigger()) {
		    showMenu(e);
		}
	    }

	    public void mouseReleased(MouseEvent e) {
		if (e.isPopupTrigger()) {
		    showMenu(e);
		}
	    }

	    private void showMenu(MouseEvent e) {
		popup.show(e.getComponent(), e.getX(), e.getY());
	    }

	    @Override
	    public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
		    if (jList.getSelectedValue() != null) {
			Main.newViewer(jList.getSelectedValue(), Mode.VIEW);
		    }
		}
	    }
	});
    }
}