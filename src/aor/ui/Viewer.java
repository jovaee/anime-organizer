package aor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import aor.graphics.Theme;
import aor.main.Main;
import aor.objects.Anime;
import aor.objects.enums.Mode;
import aor.objects.enums.State;
import aor.ui.components.UpdatePanel;
import aor.ui.components.ViewPanel;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowStateListener;
import java.awt.event.WindowEvent;

/**
 * @author Asmicor
 *
 */
public class Viewer extends JFrame {

    private static final long serialVersionUID = 6352057724186352666L;

    private JPanel basePanel;
    private Anime currentAnime;
    private State lastState;
    private JLabel lblDescription;
    private JLabel lblInformation;
    private String oldAltName;
    private String oldName;
    private String oldPrequel[];
    private String oldSequel[];
    private JLabel picture;
    private PSSelector psSelector;
    private JTextField tfAltName;
    private JTextField tfName;

    private JTextPane tpDescription;

    private File pictureFile;
    private JScrollPane scrollPane_3;

    private ViewPanel viewPanel;
    private UpdatePanel updatePanel;
    private JLabel lblSwitchMode;

    private Mode mode;

    /**
     * Create the frame.
     * 
     * @wbp.parser.constructor
     */
    public Viewer(Anime anime, Mode mode) {
    	this.currentAnime = anime;
	this.oldName = anime.getName();
	this.lastState = anime.getState();
	this.oldPrequel = anime.getPrequel();
	this.oldSequel = anime.getSequel();

	this.mode = mode;

	setTitle("Anime Organizer - Viewer for " + anime.getName());
	initialize();
    }

    public Viewer(Mode mode) {
	this.mode = mode;

	setTitle("Anime Organizer - Creating new Anime");
	initialize();
    }

    private void initialize() {
	setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	setBounds(100, 100, 900, 778);
	setLocationRelativeTo(null);

	JPanel contentPane = new JPanel();
	contentPane.setAutoscrolls(true);
	contentPane.setBackground(Color.WHITE);
	contentPane.setBorder(null);
	setContentPane(contentPane);

	setVisible(true);
	setLocationRelativeTo(null);

	JScrollPane scrollPane_2 = new JScrollPane();
	scrollPane_2.setBorder(null);
	scrollPane_2.setBackground(Color.WHITE);

	basePanel = new JPanel();
	basePanel.setMaximumSize(new Dimension(890, 805));
	basePanel.setBorder(null);
	basePanel.setBackground(Color.WHITE);

	tfName = new JTextField("...");
	tfName.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusLost(FocusEvent arg0) {
		if (tfName.getText().equals("")) {
		    tfName.setText(currentAnime.getName());
		}
	    }
	});
	tfName.setEditable(false);
	tfName.setToolTipText("Name of this anime");
	tfName.setDoubleBuffered(true);
	tfName.setHorizontalAlignment(SwingConstants.CENTER);
	tfName.setOpaque(true);
	tfName.setForeground(Color.WHITE);
	tfName.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
	tfName.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
	tfName.setBackground(new Color(0, 102, 153));

	tfAltName = new JTextField("...");
	tfAltName.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusGained(FocusEvent arg0) {
		if (tfAltName.getText().equals("...")) {
		    tfAltName.setText("");
		}
	    }

	    @Override
	    public void focusLost(FocusEvent arg0) {
		if (tfAltName.getText().equals("")) {
		    tfAltName.setText(currentAnime.getAltName());
		}
	    }
	});
	tfAltName.setEditable(false);
	tfAltName.setToolTipText("Alternative name this anime");
	tfAltName.setDoubleBuffered(true);
	tfAltName.setBorder(new LineBorder(new Color(0, 0, 0), 2, true));
	tfAltName.setFont(new Font("Segoe UI Semibold", Font.BOLD, 13));
	tfAltName.setOpaque(true);
	tfAltName.setBackground(new Color(0, 102, 153));
	tfAltName.setHorizontalAlignment(SwingConstants.CENTER);
	tfAltName.setForeground(Color.WHITE);

	lblDescription = new JLabel("Desription:");
	lblDescription.setHorizontalAlignment(SwingConstants.CENTER);
	lblDescription.setOpaque(true);
	lblDescription.setFont(new Font("Segoe UI Semibold", Font.BOLD, 13));
	lblDescription.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	lblDescription.setBackground(new Color(100, 149, 237));

	JPanel pnlDescription = new JPanel();
	pnlDescription.setBackground(Color.WHITE);
	pnlDescription.setBorder(null);
	pnlDescription.setLayout(new BorderLayout(0, 0));

	picture = new JLabel("");
	picture.setRequestFocusEnabled(false);
	picture.setBackground(Color.WHITE);
	picture.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	picture.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent arg0) {
		if (mode == Mode.UPDATE || mode == Mode.CREATE) {
		    JFileChooser chooser = new JFileChooser();
		    chooser.showOpenDialog(picture);

		    if (chooser.getSelectedFile() != null) {
			pictureFile = chooser.getSelectedFile();
			picture.setIcon(new ImageIcon(pictureFile.toString()));
		    }
		}
	    }
	});
	picture.setHorizontalAlignment(SwingConstants.CENTER);

	lblInformation = new JLabel("Information:");
	lblInformation.setHorizontalAlignment(SwingConstants.CENTER);
	lblInformation.setAlignmentX(Component.CENTER_ALIGNMENT);
	lblInformation.setOpaque(true);
	lblInformation.setFont(new Font("Segoe UI Semibold", Font.BOLD, 13));
	lblInformation.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	lblInformation.setBackground(new Color(100, 149, 237));

	JScrollPane scrollPane_1 = new JScrollPane();
	scrollPane_1.setBorder(null);
	scrollPane_1.setBackground(Color.WHITE);
	pnlDescription.add(scrollPane_1);

	tpDescription = new JTextPane();
	scrollPane_1.setViewportView(tpDescription);
	tpDescription.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusGained(FocusEvent arg0) {
		if (tpDescription.isEditable()) {
		    if (tpDescription.getText().equals("No Description Available")) {
			tpDescription.setText("");
		    }
		}
	    }

	    @Override
	    public void focusLost(FocusEvent e) {
		if (tpDescription.isEditable()) {
		    if (tpDescription.getText().equals("")) {
			tpDescription.setText("No Description Available");
		    }
		}
	    }
	});
	contentPane.setLayout(new BorderLayout(0, 0));
	tpDescription.setToolTipText("");
	tpDescription.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	tpDescription.setEditable(false);
	tpDescription.setBackground(Color.WHITE);

	scrollPane_3 = new JScrollPane();
	scrollPane_3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	scrollPane_3.setViewportBorder(null);
	scrollPane_3.setBorder(null);
	scrollPane_3.setBackground(Color.WHITE);
	scrollPane_3.setPreferredSize(null);

	lblSwitchMode = new JLabel("Update");
	lblSwitchMode.addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseClicked(MouseEvent arg0) {
		if (mode == Mode.VIEW) {
		    updatePanel = new UpdatePanel(currentAnime, mode);

		    scrollPane_3.setViewportView(updatePanel);
		    lblSwitchMode.setText("View");
		    mode = Mode.UPDATE;
		} else {
		    viewPanel = new ViewPanel(currentAnime);

		    scrollPane_3.setViewportView(viewPanel);
		    lblSwitchMode.setText("Update");
		    mode = Mode.VIEW;
		}

		getThis().revalidate();
		getThis().repaint();

		revalidate();
		repaint();
	    }
	});
	lblSwitchMode.setOpaque(true);
	lblSwitchMode.setHorizontalAlignment(SwingConstants.CENTER);
	lblSwitchMode.setFont(new Font("Segoe UI Semibold", Font.BOLD, 13));
	lblSwitchMode.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	lblSwitchMode.setBackground(new Color(100, 149, 237));
	lblSwitchMode.setAlignmentX(0.5f);

	if (mode == Mode.VIEW) {
	    viewPanel = new ViewPanel(currentAnime);
	    scrollPane_3.setViewportView(viewPanel);
	} else if (mode == Mode.UPDATE || mode == Mode.CREATE) {
	    updatePanel = new UpdatePanel(currentAnime, mode);
	    scrollPane_3.setViewportView(updatePanel);

	    setEditing(true);
	}

	scrollPane_2.setViewportView(basePanel);
	GroupLayout gl_basePanel = new GroupLayout(basePanel);
	gl_basePanel.setHorizontalGroup(gl_basePanel.createParallelGroup(Alignment.LEADING)
		.addGroup(gl_basePanel.createSequentialGroup()
			.addGroup(gl_basePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_basePanel.createSequentialGroup().addGap(10).addComponent(tfName,
					GroupLayout.DEFAULT_SIZE, 864, Short.MAX_VALUE))
				.addGroup(gl_basePanel.createSequentialGroup().addGap(20)
					.addGroup(
						gl_basePanel.createParallelGroup(Alignment.TRAILING)
							.addGroup(gl_basePanel.createSequentialGroup()
								.addComponent(tfAltName, GroupLayout.DEFAULT_SIZE, 645,
									Short.MAX_VALUE)
								.addGap(3))
							.addGroup(gl_basePanel.createSequentialGroup()
								.addComponent(lblDescription, GroupLayout.DEFAULT_SIZE,
									645, Short.MAX_VALUE)
								.addGap(3))
							.addComponent(pnlDescription, GroupLayout.DEFAULT_SIZE, 648,
								Short.MAX_VALUE))
					.addGap(8)
					.addComponent(picture, GroupLayout.PREFERRED_SIZE, 190,
						GroupLayout.PREFERRED_SIZE)
					.addGap(8))
				.addGroup(
					gl_basePanel.createSequentialGroup().addGap(21)
						.addComponent(lblInformation, GroupLayout.DEFAULT_SIZE, 783,
							Short.MAX_VALUE)
						.addGap(1)
						.addComponent(lblSwitchMode, GroupLayout.PREFERRED_SIZE, 61,
							GroupLayout.PREFERRED_SIZE)
						.addGap(8))
				.addGroup(gl_basePanel.createSequentialGroup().addGap(21)
					.addComponent(scrollPane_3, GroupLayout.DEFAULT_SIZE, 845, Short.MAX_VALUE)
					.addGap(8)))
			.addGap(10)));
	gl_basePanel.setVerticalGroup(gl_basePanel.createParallelGroup(Alignment.LEADING)
		.addGroup(gl_basePanel.createSequentialGroup().addGap(11)
			.addComponent(tfName, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(6)
			.addGroup(gl_basePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_basePanel.createSequentialGroup()
					.addComponent(tfAltName, GroupLayout.PREFERRED_SIZE, 30,
						GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(lblDescription, GroupLayout.PREFERRED_SIZE,
						30, GroupLayout.PREFERRED_SIZE)
					.addGap(6).addComponent(pnlDescription, GroupLayout.PREFERRED_SIZE, 236,
						GroupLayout.PREFERRED_SIZE))
				.addComponent(picture, GroupLayout.PREFERRED_SIZE, 308, GroupLayout.PREFERRED_SIZE))
			.addGap(6)
			.addGroup(gl_basePanel.createParallelGroup(Alignment.LEADING)
				.addComponent(lblInformation, GroupLayout.PREFERRED_SIZE, 27,
					GroupLayout.PREFERRED_SIZE)
				.addComponent(lblSwitchMode, GroupLayout.PREFERRED_SIZE, 27,
					GroupLayout.PREFERRED_SIZE))
			.addPreferredGap(ComponentPlacement.RELATED)
			.addComponent(scrollPane_3, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE).addContainerGap()));
	basePanel.setLayout(gl_basePanel);
	contentPane.add(scrollPane_2);

	setVisible(true);
	
	if (mode != Mode.CREATE) {
	    updateInfo();
	}	
    }

    private JFrame getThis() {
	return this;
    }

    private void setEditing(boolean set) {
	tpDescription.setEditable(set);
    }

    private void setLabelColor(State state) {
	// Change label color according to the state of the anime
	if (state == State.WATCHING) {
	    tfName.setBackground(Theme.WAITING_BASE_COLOR);
	    tfAltName.setBackground(Theme.WAITING_BASE_COLOR);
	    lblDescription.setBackground(Theme.WAITING_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.WAITING_ACCENT_COLOR);
	} else if (state == State.FINISHED) {
	    tfName.setBackground(Theme.FINISHED_BASE_COLOR);
	    tfAltName.setBackground(Theme.FINISHED_BASE_COLOR);
	    lblDescription.setBackground(Theme.FINISHED_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.FINISHED_ACCENT_COLOR);
	} else if (state == State.DROPPED) {
	    tfName.setBackground(Theme.DROPPED_BASE_COLOR);
	    tfAltName.setBackground(Theme.DROPPED_BASE_COLOR);
	    lblDescription.setBackground(Theme.DROPPED_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.DROPPED_ACCENT_COLOR);
	} else if (state == State.STALLED) {
	    tfName.setBackground(Theme.STALLED_BASE_COLOR);
	    tfAltName.setBackground(Theme.STALLED_BASE_COLOR);
	    lblDescription.setBackground(Theme.STALLED_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.STALLED_ACCENT_COLOR);
	} else if (state == State.PLANNING_TO_WATCH) {
	    tfName.setBackground(Theme.PLANNING_BASE_COLOR);
	    tfAltName.setBackground(Theme.PLANNING_BASE_COLOR);
	    lblDescription.setBackground(Theme.PLANNING_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.PLANNING_ACCENT_COLOR);
	} else {
	    tfName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    tfAltName.setBackground(Theme.UPCOMMING_BASE_COLOR);
	    lblDescription.setBackground(Theme.UPCOMMING_ACCENT_COLOR);
	    lblInformation.setBackground(Theme.UPCOMMING_ACCENT_COLOR);
	}
    }

    public void updateInfo() {
	setTitle("Anime Organizer - Viewer for " + currentAnime.getName());

	tfName.setText(currentAnime.getName());

	tfAltName.setText(currentAnime.getAltName());

	if (currentAnime.getDescription().equals("...")) {
	    tpDescription.setText("No Description Available");
	} else {
	    tpDescription.setText(currentAnime.getDescription());
	}

	if (currentAnime.getAnimeArt().equals("...")) {
	    picture.setIcon(new ImageIcon(
		    Toolkit.getDefaultToolkit().getImage(getClass().getResource(Anime.ANIME_ART_EMPTY_VALUE))
			    .getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_SMOOTH)));
	} else {
	    picture.setIcon(new ImageIcon(Toolkit.getDefaultToolkit()
		    .getImage(System.getProperty("user.home") + "\\Documents\\Anime Organizer\\Pictures\\"
			    + currentAnime.getAnimeArt())
		    .getScaledInstance(picture.getWidth(), picture.getHeight(), Image.SCALE_SMOOTH)));
	}

	setLabelColor(currentAnime.getState());
    }

    public void viewNemAnime(Anime anime) {
	currentAnime = anime;

	updateInfo();
	viewPanel.changeAnime(currentAnime);
    }

    public void updatePS(boolean reset, String state, String name) {
	/*
	 * if (state.equals("Prequel")) { if (reset) {
	 * lblPrequelData.setText("..."); } else { lblPrequelData.setText(name);
	 * } } else { if (reset) { lblSequelData.setText("..."); } else {
	 * lblSequelData.setText(name); } }
	 */
    }

    private void checkState() {
	/*
	 * if (cmbState.getSelectedItem().equals("Finished") ||
	 * cmbState.getSelectedItem().equals("Upcomming")) {
	 * chkbOngoing.setSelected(false); cmbAirDay.setSelectedIndex(0);
	 * cmbAirDay.setEnabled(false);
	 * 
	 * if (cmbState.getSelectedItem().equals("Upcomming")) {
	 * cmbDay.setVisible(true); cmbMonth.setVisible(true);
	 * cmbYear.setVisible(true);
	 * 
	 * cmbDay.setSelectedIndex(currentAnime.getDay()); if
	 * (currentAnime.getMonth() == 0) { cmbMonth.setSelectedIndex(0); } else
	 * { cmbMonth.setSelectedIndex(currentAnime.getMonth()); }
	 * 
	 * if (currentAnime.getYear() == 0) { cmbYear.setSelectedIndex(0); }
	 * else { cmbYear.setSelectedItem(currentAnime.getYear()); } } else {
	 * cmbDay.setVisible(false); cmbMonth.setVisible(false);
	 * cmbYear.setVisible(false); } }
	 * 
	 * if (pnlUpdate.isVisible() &&
	 * (cmbState.getSelectedItem().equals("Upcomming") ||
	 * cmbState.getSelectedItem().equals("Watching") ||
	 * cmbState.getSelectedItem().equals("Planning to watch"))) { if
	 * (cmbState.getSelectedItem().equals("Planning to watch") ||
	 * cmbState.getSelectedItem().equals("Upcomming")) {
	 * btnEpisodeMinus.setEnabled(false); btnEpisodePlus.setEnabled(false);
	 * tfEpisodeUpdate.setEnabled(false);
	 * tfEpisodeUpdate.setEditable(false);
	 * 
	 * tfEpisodeUpdate.setText("0"); } else {
	 * btnEpisodeMinus.setEnabled(true); btnEpisodePlus.setEnabled(true);
	 * tfEpisodeUpdate.setEnabled(true); tfEpisodeUpdate.setEditable(true);
	 * }
	 * 
	 * btnRatingMinus.setEnabled(false); btnRatingPlus.setEnabled(false);
	 * lblRatingUpdate.setEnabled(false); lblRatingUpdate.setText("5.0"); }
	 * else if (pnlUpdate.isVisible()) { btnEpisodeMinus.setEnabled(true);
	 * btnEpisodePlus.setEnabled(true); tfEpisodeUpdate.setEnabled(true);
	 * tfEpisodeUpdate.setEditable(true);
	 * 
	 * btnRatingMinus.setEnabled(true); btnRatingPlus.setEnabled(true);
	 * lblRatingUpdate.setEnabled(true);
	 * lblRatingUpdate.setText(Double.toString(currentAnime.getRating())); }
	 * 
	 * if (lastState == State.UPCOMMING || lastState ==
	 * State.PLANNING_TO_WATCH) {
	 * tfEpisodeUpdate.setText(lblEpisodeData.getText()); }
	 * 
	 * if (!cmbState.getSelectedItem().equals(lastState)) { switch ((String)
	 * cmbState.getSelectedItem()) { case "Watching": lastState =
	 * State.WATCHING; break; case "Finished": lastState = State.FINISHED;
	 * break; case "Dropped": lastState = State.DROPPED; break; case
	 * "Stalled": lastState = State.STALLED; break; case "Planning to watch"
	 * : lastState = State.PLANNING_TO_WATCH; break; default: lastState =
	 * State.UPCOMMING; break; } }
	 */
    }

    private void checkOngoing() {
	/*
	 * if (!cmbState.getSelectedItem().equals(State.UPCOMMING.toString()) &&
	 * !cmbState.getSelectedItem().equals(State.FINISHED.toString())) { if
	 * (chkbOngoing.isSelected()) { cmbAirDay.setEnabled(true); } else {
	 * cmbAirDay.setEnabled(false); cmbAirDay.setSelectedIndex(0); } } else
	 * { chkbOngoing.setSelected(false); cmbAirDay.setEnabled(false);
	 * cmbAirDay.setSelectedIndex(0); }
	 */
    }

    private void openUpdate() {

	/*
	 * if (!pnlUpdate.isVisible()) { oldName = tfName.getText(); oldAltName
	 * = tfAltName.getText();
	 * 
	 * if (lblPrequelData.getText().equals("...")) { btnPrequel.setText(
	 * "Select Prequel"); } else { btnPrequel.setText("Change Prequel"); }
	 * 
	 * if (lblSequelData.getText().equals("...")) { btnSequel.setText(
	 * "Select Sequel"); } else { btnSequel.setText("Change Sequel"); }
	 * 
	 * tfEpisodeUpdate.setText(lblEpisodeData.getText()); if
	 * (lblLinkData.getText().equals("...")) { tfLink.setText(""); } else {
	 * tfLink.setText(lblLinkData.getText()); }
	 * 
	 * if (lblStateData.getText().equals(State.UPCOMMING.toString()) ||
	 * lblStateData.getText().equals(State.PLANNING_TO_WATCH.toString()) ||
	 * lblStateData.getText().equals(State.WATCHING.toString())) { if
	 * (cmbState.getSelectedItem().equals(State.PLANNING_TO_WATCH.toString()
	 * ) || cmbState.getSelectedItem().equals(State.UPCOMMING.toString())) {
	 * btnEpisodeMinus.setEnabled(false); btnEpisodePlus.setEnabled(false);
	 * tfEpisodeUpdate.setEnabled(false);
	 * tfEpisodeUpdate.setEditable(false); }
	 * 
	 * if (lblStateData.getText().equals(State.UPCOMMING.toString())) {
	 * cmbDay.setSelectedIndex(currentAnime.getDay()); if
	 * (currentAnime.getMonth() == 0) { cmbMonth.setSelectedIndex(0); } else
	 * { cmbMonth.setSelectedIndex(currentAnime.getMonth()); }
	 * 
	 * if (currentAnime.getYear() == 0) { cmbYear.setSelectedIndex(0); }
	 * else { cmbYear.setSelectedItem(currentAnime.getYear()); }
	 * 
	 * cmbDay.setVisible(true); cmbMonth.setVisible(true);
	 * cmbYear.setVisible(true); }
	 * 
	 * if (lblStateData.getText().equals(State.UPCOMMING.toString()) ||
	 * lblStateData.getText().equals(State.PLANNING_TO_WATCH.toString())) {
	 * btnEpisodeMinus.setEnabled(false); btnEpisodePlus.setEnabled(false);
	 * tfEpisodeUpdate.setEnabled(false); tfEpisodeUpdate.setText("0"); }
	 * 
	 * lblRatingUpdate.setText("5.0"); btnRatingMinus.setEnabled(false);
	 * btnRatingPlus.setEnabled(false); lblRatingUpdate.setEnabled(false); }
	 * else { btnEpisodeMinus.setEnabled(true);
	 * btnEpisodePlus.setEnabled(true); tfEpisodeUpdate.setEnabled(true);
	 * tfEpisodeUpdate.setEditable(true);
	 * 
	 * lblRatingUpdate.setText(Float.toString(currentAnime.getRating()));
	 * btnRatingMinus.setEnabled(true); btnRatingPlus.setEnabled(true);
	 * lblRatingUpdate.setEnabled(true); }
	 * 
	 * if (lblTotalEpisodesData.getText().equals("...")) {
	 * tfTotalEpisodesUpdate.setText("0"); } else {
	 * tfTotalEpisodesUpdate.setText(lblTotalEpisodesData.getText()); }
	 * tfEpisodeTimeUpdate.setText(Integer.toString(currentAnime.
	 * getEpisodeRuntime()));
	 * 
	 * cmbType.setSelectedItem(lblTypeData.getText());
	 * cmbState.setSelectedItem(lblStateData.getText());
	 * 
	 * if (lblAirDayData.getText().equals("...")) { if
	 * (currentAnime.isOngoing()) { chkbOngoing.setSelected(true);
	 * cmbAirDay.setEnabled(true); } else { chkbOngoing.setSelected(false);
	 * cmbAirDay.setEnabled(false); } } else if
	 * (cmbState.getSelectedItem().equals(State.FINISHED.toString()) ||
	 * cmbState.getSelectedItem().equals(State.UPCOMMING.toString())) {
	 * chkbOngoing.setSelected(false); cmbAirDay.setEnabled(false); } else {
	 * chkbOngoing.setSelected(true); cmbAirDay.setEnabled(true);
	 * cmbAirDay.setSelectedItem(lblAirDayData.getText()); }
	 * 
	 * btnUpdate.setText("Cancel Update"); } else {
	 * btnUpdate.setText("Update");
	 * 
	 * //lblSequelData.setText(currentAnime.getSequel());
	 * //lblPrequelData.setText(currentAnime.getPrequel());
	 * 
	 * cmbDay.setVisible(false); cmbMonth.setVisible(false);
	 * cmbYear.setVisible(false);
	 * 
	 * if (currentAnime.getAnimeArt().equals(Anime.ANIME_ART_EMPTY_VALUE)) {
	 * picture.setIcon(new
	 * ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource
	 * (Anime.ANIME_ART_EMPTY_VALUE)).getScaledInstance(picture.getWidth(),
	 * picture.getHeight(), Image.SCALE_SMOOTH))); } else {
	 * picture.setIcon(new
	 * ImageIcon(Toolkit.getDefaultToolkit().getImage(currentAnime.
	 * getAnimeArt()).getScaledInstance(picture.getWidth(),
	 * picture.getHeight(), Image.SCALE_SMOOTH))); } }
	 * 
	 * tfName.setEditable(!tfName.isEditable());
	 * tfAltName.setEditable(!tfAltName.isEditable());
	 * tpDescription.setEditable(!tpDescription.isEditable());
	 * 
	 * pnlUpdate.setVisible(!pnlUpdate.isVisible());
	 * lblUpdate.setVisible(!lblUpdate.isVisible());
	 */

	/*
	 * if (currentAnime.getAnimeArt().equals(Anime.ANIME_ART_EMPTY_VALUE)) {
	 * picture.setVisible(!picture.isVisible());
	 * picture.setEnabled(!picture.isEnabled()); }
	 */

	// lastState = currentAnime.getState();
    }
}
