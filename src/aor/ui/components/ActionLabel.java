/**
 * 
 */
package aor.ui.components;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;

import aor.main.Main;

/**
 * @author Johann van Eeden
 *
 */
public class ActionLabel extends JLabel {

	private static final long serialVersionUID = 3731772901105197424L;
	
	public ActionLabel(String text) {
		super(text);
		
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				if (getText().equals("...")) {
					return;
				}
				
				Main.viewNewAnime(getText());				
			}
		});		
	}
}