package aor.ui.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;

import javax.swing.ButtonModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * @author Asmicor
 *
 */
public class JDateChooser extends JPanel {
	private static final long serialVersionUID = 5207756541994043712L;

	private JLabel txtFebuary;
	
	private int day;
	private int month;
	private int year;
	
	/**
	 * Create the panel.
	 */
	public JDateChooser(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
		
		initialize();
	}
	
	public JDateChooser() {
		setBackground(Color.WHITE);
		day = Calendar.getInstance().get(Calendar.DATE);
		month = Calendar.getInstance().get(Calendar.MONTH) + 1;
		year = Calendar.getInstance().get(Calendar.YEAR);		
		
		initialize();		
		setText();
	}
	
	private void initialize() {
		setBorder(null);
		
		JButton btnYearMin = new JButton(" <<< ");
		btnYearMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				year = year - 1;
			}
		});
		btnYearMin.setAlignmentY(0.0f);
		btnYearMin.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		btnYearMin.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		final ButtonModel bModel = btnYearMin.getModel();
	    bModel.addChangeListener(new ChangeListener() {
	    	final Timer timer = new Timer(100 , new ActionListener() {

	    			@Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            year = year - 1;
	    	            
	    	            setText();
	    	         }
	    	      });

				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel.isPressed() && !timer.isRunning()) {
			               timer.start();
			        } else if (!bModel.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		JButton btnMonthMin = new JButton("<<");
		btnMonthMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (month == 0) {
	            	month = 12;
	            } else {
	            	month = month - 1;
	            }
	            
	            setText();
			}
		});
		btnMonthMin.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		btnMonthMin.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		final ButtonModel bModel_7 = btnMonthMin.getModel();
	      bModel_7.addChangeListener(new ChangeListener() {
	    	  final Timer timer = new Timer(100 , new ActionListener() {

	    	         @Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            if (month == 0) {
	    	            	month = 12;
	    	            } else {
	    	            	month = month - 1;
	    	            }
	    	            
	    	            setText();
	    	         }
	    	      });

				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel_7.isPressed() && !timer.isRunning()) {
			               timer.start();
			        	} else if (!bModel.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		JButton btnDayMin = new JButton("<");
		btnDayMin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (day == 0) {
	            	day = 31;
	            } else {
	            	day = day - 1;
	            }
	            
	            setText();
			}
		});
		btnDayMin.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		btnDayMin.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		final ButtonModel bModel_1 = btnDayMin.getModel();
	      bModel_1.addChangeListener(new ChangeListener() {
	    	  final Timer timer = new Timer(100 , new ActionListener() {

	    	         @Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            if (day == 0) {
	    	            	day = 31;
	    	            } else {
	    	            	day = day - 1;
	    	            }
	    	            
	    	            setText();
	    	         }
	    	      });

				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel_1.isPressed() && !timer.isRunning()) {
			               timer.start();
			        	} else if (!bModel_1.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		txtFebuary = new JLabel();
		txtFebuary.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		txtFebuary.setHorizontalAlignment(SwingConstants.CENTER);
		
		JButton btnDayInc = new JButton(">");
		btnDayInc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (day == 31) {
	            	day = 0;
	            } else {
	            	day = day + 1;
	            }
	            
	            setText();
			}
		});
		btnDayInc.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		btnDayInc.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		final ButtonModel bModel_2 = btnDayInc.getModel();
	      bModel_2.addChangeListener(new ChangeListener() {
	    	  final Timer timer = new Timer(100 , new ActionListener() {

	    	         @Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            if (day == 31) {
	    	            	day = 0;
	    	            } else {
	    	            	day = day + 1;
	    	            }
	    	            
	    	            setText();
	    	         }
	    	      });

				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel_2.isPressed() && !timer.isRunning()) {
			               timer.start();
			        	} else if (!bModel_2.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		
		JButton btnMonthInc = new JButton(">>");
		btnMonthInc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (month == 12) {
	            	month = 0;
	            } else {
	            	month = month + 1;
	            }
	            
	            setText();
			}
		});
		btnMonthInc.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		btnMonthInc.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		final ButtonModel bModel_3 = btnMonthInc.getModel();
	      bModel_3.addChangeListener(new ChangeListener() {
	    	  final Timer timer = new Timer(100 , new ActionListener() {

	    	         @Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            if (month == 12) {
	    	            	month = 0;
	    	            } else {
	    	            	month = month + 1;
	    	            }
	    	            
	    	            setText();
	    	         }
	    	      });
	
				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel_3.isPressed() && !timer.isRunning()) {
			               timer.start();
			        } else if (!bModel_3.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		JButton btnYearInc = new JButton(">>>");
		btnYearInc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				year = year + 1;
	            
	            setText();
			}
		});
		btnYearInc.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		btnYearInc.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		final ButtonModel bModel_4 = btnYearInc.getModel();
	    bModel_4.addChangeListener(new ChangeListener() {
	    	final Timer timer = new Timer(100 , new ActionListener() {

	    			@Override
	    	         public void actionPerformed(ActionEvent e) {
	    	            year = year + 1;
	    	            
	    	            setText();
	    	         }
	    	      });

				@Override
				public void stateChanged(ChangeEvent e) {
					if (bModel_4.isPressed() && !timer.isRunning()) {
			               timer.start();
			        } else if (!bModel_4.isPressed() && timer.isRunning()) {
			               timer.stop();
			        }				
				}
	      });
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(btnYearMin, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(btnMonthMin, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(btnDayMin, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(txtFebuary, GroupLayout.DEFAULT_SIZE, 119, Short.MAX_VALUE)
					.addGap(6)
					.addComponent(btnDayInc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(btnMonthInc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addComponent(btnYearInc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addGap(2))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(1)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnYearMin, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE)
							.addGap(2))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnMonthMin, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE)
							.addGap(2))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnDayMin, GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
							.addGap(1))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnMonthInc, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE)
							.addGap(2))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnYearInc, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE)
							.addGap(2))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(txtFebuary, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
								.addComponent(btnDayInc, GroupLayout.PREFERRED_SIZE, 18, Short.MAX_VALUE))
							.addGap(2)))
					.addGap(1))
		);
		setLayout(groupLayout);
	}
	
	/**
	 * Parses the day, month and year and displays the appropriate text
	 */
	private void setText() {
		if (day == 0) {			
			txtFebuary.setText(parseMonth() + " " + Integer.toString(year));			
		} else if (month == 0) {
			txtFebuary.setText(Integer.toString(year));
		} else {
			txtFebuary.setText(Integer.toString(day) + " " + parseMonth() + " " + Integer.toString(year));
		}
	}
	
	/**
	 * Changes the month from an integer too a String
	 * @return A string representation of the month
	 */
	private String parseMonth() {
		
		switch (month) {
			case 1: return "January";
			case 2: return "Febuary";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";
			
			default: return "";
		}		
	}
	
	public int getDay() {
		if (month == 0) {
			return 0;
		} else {
			return day;
		}		
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getYear() {
		return year;
	}	
}