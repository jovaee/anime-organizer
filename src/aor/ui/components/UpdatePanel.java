package aor.ui.components;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import aor.objects.Anime;
import aor.objects.enums.Mode;
import net.miginfocom.swing.MigLayout;

/**
 * @author Johann van Eeden
 *
 */
public class UpdatePanel extends JPanel {

    private static final long serialVersionUID = -353866865350567356L;

    private JDateChooser jdcAiredToo;
    private JDateChooser jdcAiredFrom;
    private JTextField tfLink;
    private JButton btnTypeMinus;
    private JButton btnRatingPlus;
    private JButton btnRatingMinus;
    private JLabel lblRatingData;
    private JButton btnTypePlus;
    private JButton btnStatePlus;
    private JButton btnAirDayMinus;
    private JLabel lblStateData;
    private JButton btnStateMinus;
    private JCheckBox chkbOngoing;
    private JLabel lblAirdayData;
    private JButton btnEpisodeTimePlus;
    private JButton btnAirDayPlus;
    private JButton btnEpisodeTimeMinus;
    private JLabel lblEpisodeTimeData;
    private JLabel lblTotalEpisodesData;
    private JButton btnTotalEpisodesPlus;
    private JButton btnTotalEpisodesMinus;
    private JButton btnEpisodePlus;
    private JLabel lblEpisodeData;
    private JButton btnEpisodeMinus;
    private JButton btnSelectChangeSpinoff;
    private JButton btnSelectChangeSideStory;
    private JButton btnSelectChangeAltnernativeVersion;
    private JButton btnSelectChangePrequel;
    private JButton btnSelectChangeSequel;
    private JTextArea taAltVersion;
    private JTextArea taSpinOff;
    private JTextArea taSideStory;
    private JTextArea taSequel;
    private JTextArea taPrequel;

    private void initialize(final Anime currentAnime) {
	this.setBackground(Color.WHITE);
	this.setLayout(new MigLayout("", "[][30][100,grow][][10]",
		"[10][10][10][10][10][2][10][10][10][2][10][10][10][10][10][2][10][10][grow]"));

	JLabel lblPrequels = new JLabel("Prequel");
	lblPrequels.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	this.add(lblPrequels, "cell 0 0");

	taPrequel = new JTextArea();
	taPrequel.setWrapStyleWord(true);
	taPrequel.setText("...");
	taPrequel.setTabSize(4);
	taPrequel.setLineWrap(true);
	taPrequel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	taPrequel.setEditable(false);
	taPrequel.setBorder(null);
	this.add(taPrequel, "cell 2 0,grow, wmin 10");

	btnSelectChangePrequel = new JButton("Select/Change Prequel");
	btnSelectChangePrequel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnSelectChangePrequel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnSelectChangePrequel.setAlignmentY(0.0f);
	this.add(btnSelectChangePrequel, "cell 3 0,growx");

	JLabel lblSequels = new JLabel("Sequel");
	lblSequels.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	this.add(lblSequels, "cell 0 1");

	taSequel = new JTextArea();
	taSequel.setWrapStyleWord(true);
	taSequel.setEditable(false);
	taSequel.setText("...");
	taSequel.setTabSize(4);
	taSequel.setLineWrap(true);
	taSequel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	taSequel.setBorder(null);
	this.add(taSequel, "cell 2 1,alignx left,growy");

	btnSelectChangeSequel = new JButton("Select/Change Sequel");
	btnSelectChangeSequel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnSelectChangeSequel.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnSelectChangeSequel.setAlignmentY(0.0f);
	this.add(btnSelectChangeSequel, "cell 3 1,growx");

	JLabel lblSideStories = new JLabel("Side Story");
	lblSideStories.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	this.add(lblSideStories, "cell 0 2");

	taSideStory = new JTextArea();
	taSideStory.setWrapStyleWord(true);
	taSideStory.setText("...");
	taSideStory.setTabSize(4);
	taSideStory.setLineWrap(true);
	taSideStory.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	taSideStory.setEditable(false);
	taSideStory.setBorder(null);
	this.add(taSideStory, "cell 2 2,alignx left,growy");

	btnSelectChangeSideStory = new JButton("Select/Change Side Story");
	btnSelectChangeSideStory.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnSelectChangeSideStory.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnSelectChangeSideStory.setAlignmentY(0.0f);
	this.add(btnSelectChangeSideStory, "cell 3 2,growx");

	JLabel lblSpinoffs = new JLabel("Spin-Off");
	lblSpinoffs.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	this.add(lblSpinoffs, "cell 0 3");

	taSpinOff = new JTextArea();
	taSpinOff.setWrapStyleWord(true);
	taSpinOff.setText("...");
	taSpinOff.setTabSize(4);
	taSpinOff.setLineWrap(true);
	taSpinOff.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	taSpinOff.setEditable(false);
	taSpinOff.setBorder(null);
	this.add(taSpinOff, "cell 2 3,alignx left,growy");

	btnSelectChangeSpinoff = new JButton("Select/Change Spin-Off");
	btnSelectChangeSpinoff.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnSelectChangeSpinoff.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnSelectChangeSpinoff.setAlignmentY(0.0f);
	this.add(btnSelectChangeSpinoff, "cell 3 3,growx");

	JLabel lblAltVersions = new JLabel("Alternative Version");
	lblAltVersions.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	this.add(lblAltVersions, "cell 0 4");

	taAltVersion = new JTextArea();
	taAltVersion.setWrapStyleWord(true);
	taAltVersion.setText("...");
	taAltVersion.setTabSize(4);
	taAltVersion.setLineWrap(true);
	taAltVersion.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	taAltVersion.setEditable(false);
	taAltVersion.setBorder(null);
	this.add(taAltVersion, "cell 2 4,alignx left,growy");

	btnSelectChangeAltnernativeVersion = new JButton("Select/Change Altnernative Version");
	btnSelectChangeAltnernativeVersion.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnSelectChangeAltnernativeVersion.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnSelectChangeAltnernativeVersion.setAlignmentY(0.0f);
	add(btnSelectChangeAltnernativeVersion, "cell 3 4,growx");

	JLabel lblEpisode = new JLabel("Episode");
	lblEpisode.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisode, "cell 0 6");

	btnEpisodeMinus = new JButton("-");
	btnEpisodeMinus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int tmp = Integer.parseInt(lblEpisodeData.getText());

		if (tmp > 0) {
		    tmp = tmp - 1;
		    lblEpisodeData.setText(Integer.toString(tmp));
		}
	    }
	});
	final ButtonModel bmEpisodeMinus = btnEpisodeMinus.getModel();
	bmEpisodeMinus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int tmp = Integer.parseInt(lblEpisodeData.getText());

		    if (tmp > 0) {
			tmp = tmp - 1;
			lblEpisodeData.setText(Integer.toString(tmp));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmEpisodeMinus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmEpisodeMinus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnEpisodeMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnEpisodeMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnEpisodeMinus.setAlignmentY(0.0f);
	add(btnEpisodeMinus, "flowx,cell 3 6,growx");

	JLabel lblTotalEpisodes = new JLabel("Total Episodes");
	lblTotalEpisodes.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblTotalEpisodes, "cell 0 7");

	btnTotalEpisodesMinus = new JButton("-");
	btnTotalEpisodesMinus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int tmp = Integer.parseInt(lblTotalEpisodesData.getText());

		if (tmp > 0 && tmp > Integer.parseInt(lblEpisodeData.getText())) {
		    tmp = tmp - 1;
		    lblTotalEpisodesData.setText(Integer.toString(tmp));
		}
	    }
	});
	final ButtonModel bmTotalEpisodesMinus = btnTotalEpisodesMinus.getModel();
	bmTotalEpisodesMinus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int tmp = Integer.parseInt(lblTotalEpisodesData.getText());

		    if (tmp > 0 && tmp > Integer.parseInt(lblEpisodeData.getText())) {
			tmp = tmp - 1;
			lblTotalEpisodesData.setText(Integer.toString(tmp));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmTotalEpisodesMinus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmTotalEpisodesMinus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnTotalEpisodesMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnTotalEpisodesMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnTotalEpisodesMinus.setAlignmentY(0.0f);
	add(btnTotalEpisodesMinus, "flowx,cell 3 7,growx");

	JLabel lblEpisodeTime = new JLabel("Episode Time");
	lblEpisodeTime.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisodeTime, "cell 0 8");

	btnEpisodeTimeMinus = new JButton("-");
	btnEpisodeTimeMinus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int tmp = Integer.parseInt(lblEpisodeTimeData.getText());

		if (tmp > 0) {
		    lblEpisodeTimeData.setText(Integer.toString(tmp - 1));
		}
	    }
	});
	final ButtonModel bmEpisodeTimeMinus = btnEpisodeTimeMinus.getModel();
	bmEpisodeTimeMinus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int tmp = Integer.parseInt(lblEpisodeTimeData.getText());

		    if (tmp > 0) {
			lblEpisodeTimeData.setText(Integer.toString(tmp - 1));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmEpisodeTimeMinus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmEpisodeTimeMinus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnEpisodeTimeMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnEpisodeTimeMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnEpisodeTimeMinus.setAlignmentY(0.0f);
	add(btnEpisodeTimeMinus, "flowx,cell 3 8,growx");

	JLabel lblAirday = new JLabel("Air Day");
	lblAirday.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAirday, "cell 0 10");

	chkbOngoing = new JCheckBox("");
	chkbOngoing.setToolTipText("Is the anime still airing");
	chkbOngoing.addItemListener(new ItemListener() {
	    public void itemStateChanged(ItemEvent e) {
		if (chkbOngoing.isSelected()) {
		    btnAirDayMinus.setEnabled(true);
		    btnAirDayPlus.setEnabled(true);
		    lblAirdayData.setText(Anime.parseDay(currentAnime.getAirDay()));
		} else {
		    btnAirDayMinus.setEnabled(false);
		    btnAirDayPlus.setEnabled(false);
		    lblAirdayData.setText("...");
		}
	    }
	});
	chkbOngoing.setBorder(null);
	add(chkbOngoing, "cell 2 10,alignx right");

	JLabel lblState = new JLabel("State");
	lblState.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblState, "cell 0 11");

	btnStateMinus = new JButton("-");
	btnStateMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnStateMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnStateMinus.setAlignmentY(0.0f);
	add(btnStateMinus, "flowx,cell 3 11,growx");

	JLabel lblType = new JLabel("Type");
	lblType.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblType, "cell 0 12");

	btnTypeMinus = new JButton("-");
	btnTypeMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnTypeMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnTypeMinus.setAlignmentY(0.0f);
	add(btnTypeMinus, "flowx,cell 3 12,growx");

	JLabel lblRating = new JLabel("Rating");
	lblRating.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblRating, "cell 0 13");

	btnRatingMinus = new JButton("-");
	btnRatingMinus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		float tmp = Float.parseFloat(lblRatingData.getText());

		if (tmp > 0) {
		    tmp = tmp - 0.5f;
		    lblRatingData.setText(Float.toString(tmp));
		}
	    }
	});
	final ButtonModel bmRatingMinus = btnRatingMinus.getModel();
	bmRatingMinus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    float tmp = Float.parseFloat(lblRatingData.getText());

		    if (tmp > 0) {
			tmp = tmp - 0.5f;
			lblRatingData.setText(Float.toString(tmp));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmRatingMinus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmRatingMinus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnRatingMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnRatingMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnRatingMinus.setAlignmentY(0.0f);
	add(btnRatingMinus, "flowx,cell 3 13,growx");

	JLabel lblLink = new JLabel("Link");
	lblLink.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblLink, "cell 0 14");

	lblEpisodeData = new JLabel("0");
	lblEpisodeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblEpisodeData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblEpisodeData, "cell 3 6,growx");

	btnEpisodePlus = new JButton("+");
	btnEpisodePlus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int tmp = Integer.parseInt(lblEpisodeData.getText());

		if (tmp < Integer.parseInt(lblTotalEpisodesData.getText())
			|| Integer.parseInt(lblTotalEpisodesData.getText()) == 0) {
		    tmp = tmp + 1;
		    lblEpisodeData.setText(Integer.toString(tmp));
		}
	    }
	});
	final ButtonModel bmEpisodePlus = btnEpisodePlus.getModel();
	bmEpisodePlus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int tmp = Integer.parseInt(lblEpisodeData.getText());

		    if (tmp < Integer.parseInt(lblTotalEpisodesData.getText())
			    || Integer.parseInt(lblTotalEpisodesData.getText()) == 0) {
			tmp = tmp + 1;
			lblEpisodeData.setText(Integer.toString(tmp));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmEpisodePlus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmEpisodePlus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnEpisodePlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnEpisodePlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnEpisodePlus.setAlignmentY(0.0f);
	add(btnEpisodePlus, "cell 3 6,growx");

	lblTotalEpisodesData = new JLabel("0");
	lblTotalEpisodesData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblTotalEpisodesData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblTotalEpisodesData, "cell 3 7,growx");

	btnTotalEpisodesPlus = new JButton("+");
	btnTotalEpisodesPlus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		lblTotalEpisodesData.setText(Integer.toString(Integer.parseInt(lblTotalEpisodesData.getText()) + 1));
	    }
	});
	final ButtonModel bmTotalEpisodesPlus = btnTotalEpisodesPlus.getModel();
	bmTotalEpisodesPlus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    lblTotalEpisodesData
			    .setText(Integer.toString(Integer.parseInt(lblTotalEpisodesData.getText()) + 1));

		    // TODO: Add this faster button to all other button where
		    // needed
		    if (timer.getDelay() > 40) {
			timer.setDelay(timer.getDelay() - 4);

		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmTotalEpisodesPlus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmTotalEpisodesPlus.isPressed() && timer.isRunning()) {
		    timer.stop();
		    timer.setDelay(100);
		}
	    }
	});
	btnTotalEpisodesPlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnTotalEpisodesPlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnTotalEpisodesPlus.setAlignmentY(0.0f);
	add(btnTotalEpisodesPlus, "cell 3 7,growx");

	tfLink = new JTextField();
	tfLink.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	tfLink.setForeground(Color.BLUE);
	tfLink.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(tfLink, "cell 3 14,growx");
	tfLink.setColumns(10);

	lblEpisodeTimeData = new JLabel("0");
	lblEpisodeTimeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblEpisodeTimeData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblEpisodeTimeData, "cell 3 8,growx");

	btnEpisodeTimePlus = new JButton("+");
	btnEpisodeTimePlus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		lblEpisodeTimeData.setText(Integer.toString(Integer.parseInt(lblEpisodeTimeData.getText()) + 1));
	    }
	});
	final ButtonModel bmEpisodeTimePlus = btnEpisodeTimePlus.getModel();
	bmEpisodeTimePlus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    lblEpisodeTimeData.setText(Integer.toString(Integer.parseInt(lblEpisodeTimeData.getText()) + 1));
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmEpisodeTimePlus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmEpisodeTimePlus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnEpisodeTimePlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnEpisodeTimePlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnEpisodeTimePlus.setAlignmentY(0.0f);
	add(btnEpisodeTimePlus, "cell 3 8,growx");

	btnAirDayMinus = new JButton("-");
	btnAirDayMinus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int day = Anime.parseDay(lblAirdayData.getText());

		if (day == 0) {
		    day = 7;
		} else {
		    day = day - 1;
		}

		lblAirdayData.setText(Anime.parseDay(day));
	    }
	});
	final ButtonModel bmAirDayMinus = btnAirDayMinus.getModel();
	bmAirDayMinus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int day = Anime.parseDay(lblAirdayData.getText());

		    if (day == 0) {
			day = 7;
		    } else {
			day = day - 1;
		    }

		    lblAirdayData.setText(Anime.parseDay(day));
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmAirDayMinus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmAirDayMinus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnAirDayMinus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnAirDayMinus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnAirDayMinus.setAlignmentY(0.0f);
	add(btnAirDayMinus, "flowx,cell 3 10,growx");

	lblAirdayData = new JLabel("...");
	lblAirdayData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblAirdayData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblAirdayData, "cell 3 10,growx");

	btnAirDayPlus = new JButton("+");
	btnAirDayPlus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		int day = Anime.parseDay(lblAirdayData.getText());

		if (day == 7) {
		    day = 0;
		} else {
		    day = day + 1;
		}

		lblAirdayData.setText(Anime.parseDay(day));
	    }
	});
	final ButtonModel bmAirDayPlus = btnAirDayPlus.getModel();
	bmAirDayPlus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    int day = Anime.parseDay(lblAirdayData.getText());

		    if (day == 7) {
			day = 0;
		    } else {
			day = day + 1;
		    }

		    lblAirdayData.setText(Anime.parseDay(day));
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmAirDayPlus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmAirDayPlus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnAirDayPlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnAirDayPlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnAirDayPlus.setAlignmentY(0.0f);
	add(btnAirDayPlus, "cell 3 10,growx");

	lblStateData = new JLabel("Watching");
	lblStateData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblStateData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblStateData, "cell 3 11,growx");

	JLabel lblTypeData = new JLabel("TV");
	lblTypeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblTypeData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblTypeData, "cell 3 12,growx");

	lblRatingData = new JLabel("5.0");
	lblRatingData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	lblRatingData.setHorizontalAlignment(SwingConstants.CENTER);
	add(lblRatingData, "cell 3 13,growx");

	btnStatePlus = new JButton("+");
	btnStatePlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnStatePlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnStatePlus.setAlignmentY(0.0f);
	add(btnStatePlus, "cell 3 11,growx");

	btnTypePlus = new JButton("+");
	btnTypePlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnTypePlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnTypePlus.setAlignmentY(0.0f);
	add(btnTypePlus, "cell 3 12,growx");

	btnRatingPlus = new JButton("+");
	btnRatingPlus.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		float tmp = Float.parseFloat(lblRatingData.getText());

		if (tmp < 10) {
		    tmp = tmp + 0.5f;
		    lblRatingData.setText(Float.toString(tmp));
		}
	    }
	});
	final ButtonModel bmRatingPlus = btnRatingPlus.getModel();
	bmRatingPlus.addChangeListener(new ChangeListener() {
	    final Timer timer = new Timer(100, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    float tmp = Float.parseFloat(lblRatingData.getText());

		    if (tmp < 10) {
			tmp = tmp + 0.5f;
			lblRatingData.setText(Float.toString(tmp));
		    }
		}
	    });

	    @Override
	    public void stateChanged(ChangeEvent e) {
		if (bmRatingPlus.isPressed() && !timer.isRunning()) {
		    timer.start();
		} else if (!bmRatingPlus.isPressed() && timer.isRunning()) {
		    timer.stop();
		}
	    }
	});
	btnRatingPlus.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	btnRatingPlus.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
	btnRatingPlus.setAlignmentY(0.0f);
	add(btnRatingPlus, "cell 3 13,growx");

	JLabel lblAiredFrom = new JLabel("Aired From");
	lblAiredFrom.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAiredFrom, "cell 0 16");

	JLabel lblAiredToo = new JLabel("Aired Too");
	lblAiredToo.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAiredToo, "cell 0 17");

	jdcAiredFrom = new JDateChooser();
	add(jdcAiredFrom, "cell 3 16,grow");

	jdcAiredToo = new JDateChooser();
	add(jdcAiredToo, "cell 3 17,grow");

	setVisible(true);
    }

    public UpdatePanel(Anime currentAnime, Mode mode) {
	initialize(currentAnime);

	if (mode == Mode.UPDATE) {
	    updateInfo(currentAnime);
	}
    }

    public void fit() {
	this.revalidate();
    }

    private void updateInfo(Anime currentAnime) {
	if (currentAnime.getTotalEpisodes() == 0) {
	    lblTotalEpisodesData.setText("...");
	} else {
	    lblTotalEpisodesData.setText(Integer.toString(currentAnime.getTotalEpisodes()));
	}

	/*
	 * if (currentAnime.getState() != State.FINISHED &&
	 * currentAnime.getState() != State.UPCOMMING) {
	 * lblNextEpisodeData.setText(Integer.toString(currentAnime.
	 * getCurrentEpisode() + 1)); } else {
	 * lblNextEpisodeData.setText("..."); }
	 */

	if (currentAnime.getEpisodeRuntime() == 0) {
	    lblEpisodeTimeData.setText("...");
	} else {
	    lblEpisodeTimeData.setText(Integer.toString(currentAnime.getEpisodeRuntime()) + " minutes");
	}

	if (currentAnime.getState().equals("Upcomming") || currentAnime.getState().equals("Planning to watch")
		|| currentAnime.getState().equals("Watching")) {
	    lblRatingData.setText("...");
	} else {
	    lblRatingData.setText(Double.toString(currentAnime.getRating()));
	}

	// Add prequels
	/*
	 * for (int i = 0; i < currentAnime.getPrequelSize(); i = i + 1) {
	 * pnlPrequels.add(new ActionLabel(currentAnime.getPrequelAt(i))); }
	 */

	// Add sequels
	/*
	 * for (int i = 0; i < currentAnime.getSequelSize(); i = i + 1) {
	 * pnlSequels.add(new ActionLabel(currentAnime.getSequelAt(i))); }
	 */

	// Add spin-offs
	/*
	 * for (int i = 0; i < currentAnime.getSpinoff().length; i = i + 1) {
	 * pnlSpinOffs.add(new ActionLabel(currentAnime.getSpinoff()[i])); }
	 */

	// Add side-stories
	/*
	 * for (int i = 0; i < currentAnime.getSideStory().length; i = i + 1) {
	 * pnlSideStories.add(new ActionLabel(currentAnime.getSideStory()[i]));
	 * }
	 */

	// Add alternative versions
	/*
	 * for (int i = 0; i < currentAnime.getAltVersion().length; i = i + 1) {
	 * pnlAltVersions.add(new ActionLabel(currentAnime.getAltVersion()[i]));
	 * }
	 */
    }

    public void changeAnime(Anime anime) {
	updateInfo(anime);
    }    
}