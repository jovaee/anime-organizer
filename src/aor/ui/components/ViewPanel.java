package aor.ui.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import aor.objects.Anime;
import aor.objects.enums.State;
import aor.ui.layouts.WrapLayout;
import net.miginfocom.swing.MigLayout;

/**
 * @author Johann van Eeden
 *
 */
public class ViewPanel extends OnlyVerticalScrollPanel {

    private static final long serialVersionUID = 3173971168694082821L;
    private JLabel lblEpisodeData;
    private JLabel lblNextEpisodeData;
    private JLabel lblTotalEpisodesData;
    private JLabel lblEpisodeTimeData;
    private JLabel lblAirDayData;
    private JLabel lblStateData;
    private JLabel lblTypeData;
    private JLabel lblRatingData;
    private JLabel lblLinkData;
    private JLabel label;
    private JPanel pnlPrequels;
    private JPanel pnlSequels;
    private JPanel pnlSideStories;
    private JPanel pnlSpinOffs;
    private JPanel pnlAltVersions;

    public ViewPanel(Anime currentAnime) {
    	setBorder(null);
	initialize();
	updateInfo(currentAnime);
    }

    private void initialize() {
	setBackground(Color.WHITE);
	setLayout(new MigLayout("", "[][20:20:20][10:10:10][100px:100px,grow,left][10:10:10]", "[][10][10][10][10][5:5:5][10][10][10][10][10][10][10][10][10][10][grow]"));

	JLabel lblNewLabel = new JLabel("Prequel");
	lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
	lblNewLabel.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblNewLabel, "cell 0 0,alignx left,aligny top");

	pnlPrequels = new JPanel();
	pnlPrequels.setBorder(null);
	pnlPrequels.setBackground(Color.WHITE);
	add(pnlPrequels, "cell 2 0 2 1, grow");
	WrapLayout wl_pnlPrequels = new WrapLayout();
	wl_pnlPrequels.setAlignment(FlowLayout.LEFT);
	pnlPrequels.setLayout(wl_pnlPrequels);

	JLabel lblNewLabel_1 = new JLabel("Sequel");
	lblNewLabel_1.setVerticalAlignment(SwingConstants.TOP);
	lblNewLabel_1.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblNewLabel_1, "cell 0 1,alignx left,aligny top");

	pnlSequels = new JPanel();
	pnlSequels.setBorder(null);
	pnlSequels.setBackground(Color.WHITE);
	add(pnlSequels, "cell 2 1 2 1,grow");
	WrapLayout wl_pnlSequels = new WrapLayout();
	wl_pnlSequels.setAlignment(FlowLayout.LEFT);
	pnlSequels.setLayout(wl_pnlSequels);

	JLabel lblSideStory = new JLabel("Side Story");
	lblSideStory.setVerticalAlignment(SwingConstants.TOP);
	lblSideStory.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblSideStory, "cell 0 2,alignx left,aligny top");

	pnlSideStories = new JPanel();
	pnlSideStories.setBorder(null);
	pnlSideStories.setBackground(Color.WHITE);
	add(pnlSideStories, "cell 2 2 2 1,grow");
	WrapLayout wl_pnlSideStories = new WrapLayout();
	wl_pnlSideStories.setAlignment(FlowLayout.LEFT);
	pnlSideStories.setLayout(wl_pnlSideStories);

	JLabel lblSpinoff = new JLabel("Spin-Off");
	lblSpinoff.setVerticalAlignment(SwingConstants.TOP);
	lblSpinoff.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblSpinoff, "cell 0 3,alignx left,aligny top");

	pnlSpinOffs = new JPanel();
	pnlSpinOffs.setBorder(null);
	pnlSpinOffs.setBackground(Color.WHITE);
	add(pnlSpinOffs, "cell 2 3 2 1,grow");
	WrapLayout wl_pnlSpinOffs = new WrapLayout();
	wl_pnlSpinOffs.setAlignment(FlowLayout.LEFT);
	pnlSpinOffs.setLayout(wl_pnlSpinOffs);

	JLabel lblAlternativeVersion = new JLabel("Alternative Version");
	lblAlternativeVersion.setVerticalAlignment(SwingConstants.TOP);
	lblAlternativeVersion.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAlternativeVersion, "cell 0 4,alignx left,aligny top");

	pnlAltVersions = new JPanel();
	pnlAltVersions.setBorder(null);
	pnlAltVersions.setBackground(Color.WHITE);
	add(pnlAltVersions, "cell 2 4 2 1,grow");
	WrapLayout wl_pnlAltVersions = new WrapLayout();
	wl_pnlAltVersions.setAlignment(FlowLayout.LEFT);
	pnlAltVersions.setLayout(wl_pnlAltVersions);

	JLabel lblEpisode = new JLabel("Episode");
	lblEpisode.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisode, "cell 0 6");

	lblEpisodeData = new JLabel("...");
	lblEpisodeData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblEpisodeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisodeData, "cell 3 6");

	JLabel lblNextEpisode = new JLabel("Next Episode");
	lblNextEpisode.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblNextEpisode, "cell 0 7");

	lblNextEpisodeData = new JLabel("...");
	lblNextEpisodeData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblNextEpisodeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblNextEpisodeData, "cell 3 7");

	JLabel lblTotalEpisodes = new JLabel("Total Episodes");
	lblTotalEpisodes.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblTotalEpisodes, "cell 0 8");

	lblTotalEpisodesData = new JLabel("...");
	lblTotalEpisodesData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblTotalEpisodesData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblTotalEpisodesData, "cell 3 8");

	JLabel lblEpisodeTime = new JLabel("Episode Time");
	lblEpisodeTime.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisodeTime, "cell 0 9");

	lblEpisodeTimeData = new JLabel("...");
	lblEpisodeTimeData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblEpisodeTimeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblEpisodeTimeData, "cell 3 9");

	JLabel lblAirDay = new JLabel("Air Day");
	lblAirDay.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAirDay, "cell 0 10,aligny top");

	lblAirDayData = new JLabel("...");
	lblAirDayData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblAirDayData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblAirDayData, "cell 3 10");

	JLabel lblState = new JLabel("State");
	lblState.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblState, "flowx,cell 0 11");

	lblStateData = new JLabel("...");
	lblStateData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblStateData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblStateData, "cell 3 11");

	JLabel lblType = new JLabel("Type");
	lblType.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblType, "cell 0 12");

	lblTypeData = new JLabel("...");
	lblTypeData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblTypeData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblTypeData, "cell 3 12");

	JLabel lblRating = new JLabel("Rating");
	lblRating.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblRating, "cell 0 13");

	lblRatingData = new JLabel("...");
	lblRatingData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblRatingData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblRatingData, "cell 3 13");

	JLabel lblLink = new JLabel("Link");
	lblLink.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblLink, "cell 0 14");

	lblLinkData = new JLabel("...");
	lblLinkData.setHorizontalTextPosition(SwingConstants.RIGHT);
	lblLinkData.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
	lblLinkData.setForeground(Color.BLUE);
	lblLinkData.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblLinkData, "cell 3 14");

	JLabel lblUpcommingDate = new JLabel("Upcomming Date");
	lblUpcommingDate.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(lblUpcommingDate, "cell 0 15");

	label = new JLabel("...");
	label.setHorizontalTextPosition(SwingConstants.RIGHT);
	label.setForeground(Color.BLACK);
	label.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
	add(label, "cell 3 15,growx");
    }

    private void updateInfo(Anime currentAnime) {
	if (currentAnime.getTotalEpisodes() == 0) {
	    lblTotalEpisodesData.setText("...");
	} else {
	    lblTotalEpisodesData.setText(Integer.toString(currentAnime.getTotalEpisodes()));
	}

	if (currentAnime.getState() != State.FINISHED && currentAnime.getState() != State.UPCOMMING) {
	    lblNextEpisodeData.setText(Integer.toString(currentAnime.getCurrentEpisode() + 1));
	} else {
	    lblNextEpisodeData.setText("...");
	}

	if (currentAnime.getEpisodeRuntime() == 0) {
	    lblEpisodeTimeData.setText("...");
	} else {
	    lblEpisodeTimeData.setText(Integer.toString(currentAnime.getEpisodeRuntime()) + " minutes");
	}

	if (currentAnime.getState().equals("Upcomming") || currentAnime.getState().equals("Planning to watch") || currentAnime.getState().equals("Watching")) {
	    lblRatingData.setText("...");
	} else {
	    lblRatingData.setText(Double.toString(currentAnime.getRating()));
	}

	// Add prequels
	for (int i = 0; i < currentAnime.getPrequelSize(); i = i + 1) {
	    pnlPrequels.add(new ActionLabel(currentAnime.getPrequelAt(i)));
	}

	// Add sequels
	for (int i = 0; i < currentAnime.getSequelSize(); i = i + 1) {
	    pnlSequels.add(new ActionLabel(currentAnime.getSequelAt(i)));
	}

	// Add spin-offs
	for (int i = 0; i < currentAnime.getSpinoff().length; i = i + 1) {
	    pnlSpinOffs.add(new ActionLabel(currentAnime.getSpinoff()[i]));
	}

	// Add side-stories
	for (int i = 0; i < currentAnime.getSideStory().length; i = i + 1) {
	    pnlSideStories.add(new ActionLabel(currentAnime.getSideStory()[i]));
	}

	// Add alternative versions
	for (int i = 0; i < currentAnime.getAltVersion().length; i = i + 1) {
	    pnlAltVersions.add(new ActionLabel(currentAnime.getAltVersion()[i]));
	}	
    }

    public void changeAnime(Anime anime) {
	clearPanels();
	updateInfo(anime);
    }

    private void clearPanels() {
	pnlPrequels.removeAll();
	pnlSequels.removeAll();
	pnlSideStories.removeAll();
	pnlAltVersions.removeAll();
	pnlSpinOffs.removeAll();

	repaint();
    }
}