/**
 * 
 */
package aor.ui.renderers;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;

/**
 * @author Asmicor
 *
 */
public class BoldListRenderer extends JLabel implements ListCellRenderer<String> {

	private static final long serialVersionUID = -920404875937947591L;
	
	private Font font;	
	
	public BoldListRenderer() {
		font = getFont();		
	}
	
	@Override
	public Component getListCellRendererComponent(JList list, String value, int index, boolean isSelected, boolean cellHasFocus) {
				
		if (isSelected) {			
			setFont(new Font(font.getFontName(), Font.BOLD, 12));
			setBorder(new EmptyBorder(15, 10, 15, 0));				
		} else {			
			setFont(font);
			setBorder(null);				
		}
		
		setText(value);
			
		return this;
	}
}